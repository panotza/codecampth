let peopleSalary = [
    {
        "id": 1001,
        "firstname": "Luke",
        "lastname": "Skywalker",
        "company": "Walt Disney",
        "salary": 40000
    },
    {
        "id": 1002,
        "firstname": "Tony",
        "lastname": "Stark",
        "company": "Marvel",
        "salary": 1000000
    },
    {
        "id": 1003,
        "firstname": "Somchai",
        "lastname": "Jaidee",
        "company": "Love2Work",
        "salary": 20000
    },
    {
        "id": 1004,
        "firstname": "Monkey D",
        "lastname": "Luffee",
        "company": "One Piece",
        "salary": 9000000
    },
];

let newPeopleSalary = [];
peopleSalary.forEach((person, i) => {
	let people = {};
	for(key in person) {
 		if(key !== "company") {
            people[key] = peopleSalary[i][key];
		}
    }
  newPeopleSalary.push(people)
});

console.log(newPeopleSalary);