const fs = require('fs');

function calculatePercentage(percentage, number) {
    return (percentage / 100) * number;
}

function calculateSalaryThreeYears(baseSalary, percentage) {
    let totalSalary = [];
    totalSalary[0] = baseSalary;
    totalSalary[1] = totalSalary[0] + calculatePercentage(percentage, totalSalary[0]);
    totalSalary[2] = totalSalary[1] + calculatePercentage(percentage, totalSalary[1]);
    
    return totalSalary;
}

fs.readFile('homework1.json', 'utf8', (err, data) => {
    let newPeopleData;
    if (err)
        console.error(err);
    else {
        let peopleData = JSON.parse(data);
        newPeopleData = peopleData.map((person, i) => {
            let personWithFullname = {...person, fullname: person.firstname + " " + person.lastname };
            personWithFullname.salary = calculateSalaryThreeYears(personWithFullname.salary, 10);
            return personWithFullname;
        });
    }
    console.log(newPeopleData);
    
});
