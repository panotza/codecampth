const fs = require('fs');

// t.reduce((acc,elem) => {
//     if(acc.filter((elemi)=> elemi.id == elem.id)[0])
//         acc.filter((elemi)=> elemi.id==elem.id)[0].val += elem.val;
//     else
//         acc.push(elem);
//     return acc
// }, []);

function mergeAndOverwriteById(acc, elem) {
    if(acc.filter((elemi)=> elemi.id == elem.id)[0])
        acc.filter((elemi)=> elemi.id==elem.id)[0].salary = elem.salary;
    else
        acc.push(elem);
    return acc;
}

function calculateSalary(baseSalary) {
    return baseSalary * 2;
}

fs.readFile('homework1.json', 'utf8', (err, data) => {
    if (err)
        console.error(err);
    else {
        peopleData = JSON.parse(data);
        let lowSalaryPeople = peopleData.filter((person, i) => person.salary < 100000);
        let increasedSalaryPeople = lowSalaryPeople.map(person => ({...person, salary: calculateSalary(person.salary)}));
        // push increasedSalaryPeople into peopleData for reducing (merging);
        peopleData.push(...increasedSalaryPeople);
        // Merge with reduce
        let mergedPeopleData = peopleData.reduce(mergeAndOverwriteById, []);
        // calculate total salary
        let totalSalary = mergedPeopleData.reduce((acc, person) => acc + person.salary, 0);
        console.log(totalSalary)
    }
});
