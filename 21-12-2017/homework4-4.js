const fs = require('fs');

function calculateSalary(baseSalary) {
    let salary = baseSalary.replace(/\$|,/g, ''); //remove $ and ,
    salary = parseFloat(salary) / 10;
    return "$" + salary.toFixed(2);
};

let peopleData = fs.readFileSync('homework1-4.json', 'utf8');
let people = JSON.parse(peopleData);

let malePeople = people
                    .filter(person => person.gender === 'male' && person.friends.length >= 2)
                    .map(person => {
                        let {name, gender, company, email, friends, balance} = person;
                        let row = {name, gender, company, email, friends};
                        row['balance'] = calculateSalary(balance);
                        return row;
                    });
console.log(malePeople);