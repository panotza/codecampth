const fs = require('fs');

function calculateSalary(baseSalary) {
    return baseSalary * 2;
}

fs.readFile('homework1.json', 'utf8', (err, data) => {
    if (err)
        console.error(err);
    else {
        peopleData = JSON.parse(data);
        let lowSalaryPeople = peopleData
                            .filter((person, i) => person.salary < 100000)
                            .map(person => ({...person, salary: calculateSalary(person.salary)}));
        peopleData.map(person => {
            let lowSalaryPerson = lowSalaryPeople.filter(lsp => lsp.id == person.id)[0];
            if (lowSalaryPerson)
                person.salary = lowSalaryPerson.salary;
        });
        // calculate total salary
        let totalSalary = peopleData.reduce((acc, person) => acc + person.salary, 0);
        console.log(totalSalary)
    }
});
