const fs = require('fs');

function calculateSalary(baseSalary) {
    return baseSalary * 2;
}

fs.readFile('homework1.json', 'utf8', (err, data) => {
    if (err)
        console.error(err);
    else {
        peopleData = JSON.parse(data);
        let lowSalaryPeople = peopleData.map(person => {
            if (person.salary < 100000) {
                person.salary *= 2;
                return {...person, salary: person.salary * 2};
            }
        }).filter(person => person !== undefined);
        // calculate total salary
        let totalSalary = peopleData.reduce((acc, person) => acc + person.salary, 0);
        console.log(totalSalary)
    }
});
