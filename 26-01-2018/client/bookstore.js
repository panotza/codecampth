const BASEURL = 'http://localhost:3000/products/';

const state = {};

document.addEventListener("DOMContentLoaded", function(){
    initEvent();
    initProduct();
});

async function initProduct() {
    const data = await getProducts();
    if (!data) {
        return false;
    }
    window.productList = document.getElementById("productList");
    state.products = data.products;
    state.products.forEach(p => {
        window.productList.innerHTML += addProductWrapper(p.id, p.title, p.image, p.salePrice);
    });
}

function initEvent() {
    const saveProductBtn = document.getElementById('saveProductBtn');
    const addProductBtn = document.getElementById('addProductBtn');
    const bookId = document.getElementById('bookId');
    const bookTitle = document.getElementById('bookTitle');
    const bookSalePrice = document.getElementById('bookSalePrice');
    const bookPromotionDate = document.getElementById('bookPromotionDate');
    const bookImage = document.getElementById('bookImage');
    const productMethod = document.getElementById('productMethod');
    const searchBtn = document.getElementById('searchBtn');
    const searchTxt = document.getElementById('searchTxt');

    addProductBtn.addEventListener('click', () => {
        bookId.value = '0';
        bookTitle.value = '';
        bookSalePrice.value = 0;
        bookPromotionDate.value = '';
        bookImage.value = '';
        productMethod.value = 'add';
    });

    saveProductBtn.addEventListener('click', () => {
        const product = {
            id: +bookId.value || undefined,
            title: bookTitle.value,
            image: bookImage.value,
            salePrice: +bookSalePrice.value,
            promotionDate: bookPromotionDate.value
        }

        if (productMethod.value === 'add') {
            saveProduct(product);
        } else if (productMethod.value === 'edit') {
            editProduct(product);
        }

        bookId.value = '0';
        bookTitle.value = '';
        bookSalePrice.value = 0;
        bookPromotionDate.value = '';
        bookImage.value = '';
        $('#productModal').modal('hide');
    });

    searchBtn.addEventListener('click', search);
    searchTxt.addEventListener('keyup', search);
}

async function search () {
    const term = document.getElementById('searchTxt').value;
    const showAll = !term.length;
    const { products } = await searchProduct(term);
    const divProducts = window.productList.childNodes;
    for (let i = 0; i < divProducts.length; i++) {
        let divId = divProducts[i].id;
        if (divId && divId.indexOf('product-') > -1) {
            if (showAll) {
                divProducts[i].classList.remove('hidden');
                continue;
            }
            let [, id] = divId.split('-');
            if(products.indexOf(+id) === -1) {
                divProducts[i].classList.add('hidden');
            } else {
                divProducts[i].classList.remove('hidden');
            }
        }
    }
}

async function saveProduct(product) {
    const { id } = await storeProducts(product);
    if (!id) {
        return false;
    }
    product.id = id;
    state.products.push(product);
    window.productList.innerHTML += addProductWrapper(product.id, product.title, product.image, product.salePrice);
    
}

async function editProduct(product) {
    const divProduct = document.getElementById('product-' + product.id);
    const i = findProductIndexbyId(product.id, state.products);
    if (i === undefined) return;
    const resp = await updateProduct(product);
    if (!resp) {
        return false;
    }
    state.products[i] = product;
    divProduct.innerHTML = addProduct(product.id, product.title, product.image, product.salePrice);
}

function addProductWrapper(id, title, image, price) {
    return `
    <div id="product-${id}" class="col-xs-6 col-md-3 nospace">` +
    addProduct(id, title, image, price) +
    `</div>`;
}

function addProduct(id, title, image, price) {
    return `
    <div class="card" style="overflow: hidden;">
        <h2>${title}</h2>
        <a href="#">
            <img class="card-img" src="${image}"
                alt="${title}">
        </a>
        <p>price: ${price}THB
            <a href="#" class="btn btn-success" onClick="handleEditProduct(${id})">
                <i class="fa fa-pencil" aria-hidden="true"></i>
            </a>
            <a href="#" class="btn btn-danger" onClick="handleDeleteProduct(${id})">
                <i class="fa fa-eraser" aria-hidden="true"></i>
            </a>
        </p>
    </div>`;
}

function handleEditProduct(id) {
    const i = findProductIndexbyId(id, state.products);
    if (i === undefined) return;
    const product = state.products[i];
    const bookId = document.getElementById('bookId');
    bookId.value = product.id;
    document.getElementById('bookTitle').value = product.title;
    document.getElementById('bookSalePrice').value = product.salePrice;
    document.getElementById('bookPromotionDate').value = product.promotionDate;
    document.getElementById('bookImage').value = product.image;
    document.getElementById('productMethod').value = 'edit';
    $('#productModal').modal('show');
}

async function handleDeleteProduct(id) {
    const product = document.getElementById('product-' + id);
    const isConfirm = confirm('Are you sure to delete item : ' + id);
    if (!isConfirm) {
        return false;
    }
    const resp = await removeProduct(id);
    if (!resp) {
        return false;
    }
    window.productList.removeChild(product);
    state.products = state.products.filter(p =>
        p.id !== id
        ? true
        : false
    );
}

function findProductIndexbyId(id, products) {
    let index;
    for(i = 0; i < products.length; i++) {
        if (products[i].id === id) {
            return i;
        }
    }
    return;
}

// Fetch API
async function fetchAsync(url, opts) {
    const response = await fetch(url, opts);
    if (response.status !== 200) {
        return false;
    }
    const data = await response.json();
    return data;
}

function getProducts() {
    return fetchAsync(BASEURL);
}

function storeProducts(product) {
    const opts = {
        method: 'POST',
        body: JSON.stringify(product), 
        headers: new Headers({
          'Content-Type': 'application/json'
        })
    }
    return fetchAsync(BASEURL, opts);
}

function updateProduct(product) {
    if (!product.id) {
        return false;
    }
    const opts = {
        method: 'PUT',
        body: JSON.stringify(product), 
        headers: new Headers({
          'Content-Type': 'application/json'
        })
    }
    return fetchAsync(BASEURL + product.id, opts);
}

function removeProduct(id) {
    const opts = {
        method: 'DELETE'
    }
    if (!id) {
        return false;
    }
    return fetchAsync(BASEURL + id, opts);
}

function searchProduct(term) {
    return fetchAsync(BASEURL + term);
}

// Counter
const addCart = new addCartFactory();
const timeId = setInterval(addCart, 5000);
const cartAmount = document.getElementById('cartAmount');

function addCartFactory() {
    let count = 0;
    return function() {
        cartAmount.innerText = ++count;
    }
}