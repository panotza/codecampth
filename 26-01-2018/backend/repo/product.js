module.exports = {
    async get (db) {
        const [rows] = await db.execute(`
            SELECT id, title, image, sale_price, promotion_date
            FROM products`);
        return {
            products: rows.map(p => {
                return {
                    id: p.id,
                    title: p.title,
                    image: p.image,
                    salePrice: p.sale_price,
                    promotionDate: p.promotion_date
                }
            })
        }
    },
    async getByTitle (db, title) {
        const [rows] = await db.execute(`
            SELECT id
            FROM products WHERE title LIKE ?`, ['%' + title + '%']);
        return {
            products: rows.map(p => {
                return p.id
            })
        }
    },
    async store (db, product) {
        const [row] = await db.execute(`
            INSERT INTO products (title, image, sale_price, promotion_date) VALUES
            (?, ?, ?, ?)
            `, [product.title, 
                product.image, 
                product.salePrice.length === 0 ? 0 : product.salePrice, 
                product.promotionDate.length === 0 ? null : product.promotionDate
            ]);
        return row.insertId;
    },
    async update (db, id, product) {
        const [row] = await db.execute(`
            UPDATE products SET title = ?, image = ?, sale_price = ?, promotion_date = ?
            WHERE id = ?
            `, [product.title, 
                product.image, 
                product.salePrice.length === 0 ? 0 : product.salePrice, 
                product.promotionDate.length === 0 ? null : product.promotionDate, 
                +id]);
        return row.affactRows;
    },
    async remove (db, id) {
        const [rows] = await db.execute(`
            DELETE FROM products WHERE id = ?
            `, [+id]);
        return true;
    }
}