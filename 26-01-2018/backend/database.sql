DROP DATABASE IF EXISTS bookstore;
CREATE DATABASE bookstore;
use bookstore;
CREATE TABLE products (
	id INT PRIMARY KEY AUTO_INCREMENT,
    title VARCHAR(100),
    image VARCHAR(255),
    sale_price INT NOT NULL DEFAULT 0,
    promotion_date DATETIME
);

INSERT INTO products (title, image, sale_price, promotion_date) VALUES
('CHERPRANG', 'https://bnk48-www-html.s3-ap-southeast-1.amazonaws.com/uploads/members/file-0341204af64d796fb3c715fd86ba4262.png', 100, null),
('JAN', 'https://bnk48-www-html.s3-ap-southeast-1.amazonaws.com/uploads/members/file-d4e0083b4123be76c45fa5665b49a1b8.png', 100, null),
('JENNIS', 'https://bnk48-www-html.s3-ap-southeast-1.amazonaws.com/uploads/members/file-9c4230866e3f5ccaa911f728e36091cd.png', 100, null),
('KAEW', 'https://bnk48-www-html.s3-ap-southeast-1.amazonaws.com/uploads/members/file-c2564d5b05b43648969b2d88b395883b.png', 100, null);