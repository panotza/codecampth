const repo = require('../repo');
module.exports = {
    async list (ctx) {
        const products = await repo.product.get(ctx.db);
        ctx.body = products;
    },
    async getByTitle (ctx) {
        const products = await repo.product.getByTitle(ctx.db, ctx.params.title);
        ctx.body = products;
    },
    async store (ctx) {
        const insertId = await repo.product.store(ctx.db, ctx.request.body);
        ctx.body = {
            id: insertId
        };
    },
    async update (ctx) {
        const result = await repo.product.update(ctx.db, ctx.params.id, ctx.request.body);
        ctx.body = {};
    },
    async remove (ctx) {
        const result = await repo.product.remove(ctx.db, ctx.params.id);
        ctx.body = {};
    }
}