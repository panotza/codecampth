const router = new require('koa-router')();
const { product } = require('./controller');
module.exports = router
    .get('/products', product.list)
    .get('/products/:title', product.getByTitle)
    .post('/products', product.store)
    .put('/products/:id', product.update)
    .del('/products/:id', product.remove);