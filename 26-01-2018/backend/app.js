const Koa = require('koa');
const mysql = require('mysql2/promise');
const bodyParser = require('koa-bodyparser');
const router = require('./routes');
const cors = require('koa2-cors');

const app = new Koa(); 
const config = {
    database: 'bookstore',
    user: 'root',
    password: 'example',
    host: 'localhost'
}
const pool = mysql.createPool(config);

app
  .use(cors())
  .use(connectDB)
  .use(bodyParser())
  .use(router.routes())
  .use(router.allowedMethods());

app.listen(3000);

async function connectDB(ctx, next) {
    try {
        ctx.db = await pool.getConnection();
        await next();
    } catch (err) {
        console.log(err);
    } finally {
        ctx.db.release();
    }
}