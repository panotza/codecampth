const _ = require('lodash');
class MyUtility {
    constructor() {

    }
    assign () {
        return _.assign(...arguments);
    }
    times () {
        return _.times(...arguments);
    }
    keyBy () {
        return _.keyBy(...arguments);
    }
    cloneDeep () {
        return _.cloneDeep(...arguments);
    }
    filter () {
        return _.filter(...arguments);
    }
    sortBy () {
        return _.sortBy(...arguments);
    }
}