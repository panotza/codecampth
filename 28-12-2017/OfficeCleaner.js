const {Employee} = require('./Employee');

class OfficeCleaner extends Employee {
    constructor(id, fistName, lastName, salary, dressCode) {
        super(id, fistName, lastName, salary);
        this.dressCode = dressCode;
    }
    work () {  // simulate public method
        this.clean();
        this.killCoachroach();
        this.decorateRoom();
        this.welcomeGuest();
    }
    clean() {
        console.log('cleaning');
    }
    killCoachroach() {
        console.log('killing Coachroach');
    }
    decorateRoom() {
        console.log('decorating room');
    }
    welcomeGuest() {
        console.log('welcome guest');
    }
    talk(message) {
        console.log("From OfficeCleaner:");
        console.log(message);
    }
    reportRobot(self, robotMessage) {
        if (self === undefined)
            this.talk(robotMessage);
        else
            self.talk(robotMessage);
    }   
}

module.exports.OfficeCleaner = OfficeCleaner;