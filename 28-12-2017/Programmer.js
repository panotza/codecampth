const {Employee} = require('./Employee');

class Programmer extends Employee {
    constructor(id, fistName, lastName, salary, type) {
        super(id, fistName, lastName, salary);
        this.type = type;
    }
    work () {  // simulate public method
        this.createWebsite();
        this.fixComputer();
        this.installWindows();
    }
    createWebsite() {
        console.log('creating Website');
    }

    fixComputer() {
        console.log('fixing Computer');
    }

    installWindows() {
        console.log('install Windows');
    }
    talk(message) {
        console.log("From Programmer:");
        console.log(message);
    }
    reportRobot(self, robotMessage) {
        if (self === undefined)
            this.talk(robotMessage);
        else
            self.talk(robotMessage);
    }   
}

module.exports.Programmer = Programmer;