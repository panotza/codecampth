const {CEO} = require('./CEO');
const {Employee} = require('./Employee');

const ceo = new CEO('somchai', 'sudlor', 30000, 1001, 'tshirt');
const victim = new Employee(1, "No","Name");

async function createEmployees () {
    let employees = await ceo.loadEmployees();
    employees.forEach(employee => employee.work(victim));
}

createEmployees();