const {Employee} = require('./Employee');
const {Programmer} = require('./Programmer');
const {OfficeCleaner} = require('./OfficeCleaner');
const fs = require('fs');

class CEO extends Employee {
    constructor(id, firstName, lastName, salary, dressCode) {
        super(id, firstName, lastName, salary);
        this.dressCode = dressCode;
    }
    getSalary(){  // simulate public method
        return super.getSalary()*2;
    };
    work (employee) {  // simulate public method
         this._fire(employee);
         this._hire(employee);
         this._seminar();
         this._golf();
    }
    increaseSalary(employee, newSalary) {
        if (!employee.setSalary(newSalary))
            console.log(`${employee.firstName}'s salary is less than before!!`);
        else
            console.log(`${employee.firstName}'s salary has been set to ${newSalary}`);
    }
    _fire(employee) {
        this.dressCode = 'tshirt';
        console.log(`${employee.firstName} has been fired! Dress with :${this.dressCode}`);
    }
    _hire(employee) {
        this.dressCode = 'tshirt';
        console.log(`${employee.firstName} has been hired back! Dress with :${this.dressCode}`);
    }
    _seminar() {
        this.dressCode = 'suit';
        console.log(`He is going to seminar Dress with :${this.dressCode}`);
    }
    _golf() { // simulate private method
        this.dressCode = 'golf_dress';
        console.log(`He goes to golf club to find a new connection. Dress with :${this.dressCode}`);
        
    };
    gossip(target, message) {
        console.log(`Hey ${target.firstName}, ${message}`);
    }
    async loadEmployees() {
        let data = await this._readFileP('employee9.json');
        this.employeesRaw = JSON.parse(data);
        this.employees = this.employeesRaw.map(employee => {
            let {id, firstname, lastname, salary, role, type = '', dressCode = ''} = employee;
            switch (role) {
                case 'Programmer':
                    return new Programmer(id, firstname, lastname, salary, type);
                    break;
                case 'OfficeCleaner':
                    return new OfficeCleaner(id, firstname, lastname, salary, dressCode);
                    break;
                case 'CEO':
                    return new CEO(id, firstname, lastname, salary, dressCode);
                    break;
                default:
                    console.error("unknown role was detected.");
            }
        });
        return this.employees;
    }
    _readFileP(fileName) {
        return new Promise((resolve, reject) => {
            fs.readFile(fileName, 'utf8', (err, data) => err ? reject(err) : resolve(data));
        });
    }
    talk(message) {
        console.log("From CEO:");
        console.log(message);
    }
    reportRobot(self, robotMessage) {
        if (self === undefined)
            this.talk(robotMessage);
        else
            self.talk(robotMessage);
    }
}

module.exports.CEO = CEO;