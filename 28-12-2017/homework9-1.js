const {CEO} = require('./CEO');
const fs = require('fs');
const EventEmitter = require('events');
const emitter = new EventEmitter();

const ceo = new CEO('somchai', 'sudlor', 30000, 1001, 'tshirt');

emitter.on('writeRobotFileComplete', ceo.reportRobot);

fs.readFile('file/head.txt', 'utf8', function(err, dataHead) {
    fs.readFile('file/body.txt', 'utf8', function(err, dataBody) {
        fs.readFile('file/leg.txt', 'utf8', function(err, dataLeg) {
            fs.readFile('file/feet.txt', 'utf8', function(err, dataFeet) {
                let robotData = dataHead + '\n' + dataBody + '\n' + dataLeg + '\n' + dataFeet;
                fs.writeFile('robot.txt', robotData, 'utf8', function(err, data) {
                    emitter.emit('writeRobotFileComplete', ceo, robotData);
                });
            });
        });
    });
});