SHOW DATABASES;
SHOW TABLES;

DESC current_dept_emp;
DESC departments;
DESC dept_emp;
DESC dept_emp_latest_date;
DESC dept_manager;
DESC employees;
DESC salaries;
DESC titles;

SELECT DISTINCT title FROM titles;

SELECT COUNT(*) AS total, 
(SELECT COUNT(*) AS male FROM employees WHERE gender = 'M') AS male,
(SELECT COUNT(*) AS female FROM employees WHERE gender = 'F') AS female
FROM employees;

SELECT COUNT(DISTINCT last_name) FROM employees;