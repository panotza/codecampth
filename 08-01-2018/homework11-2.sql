INSERT INTO employees (firstname, lastname, age) VALUES
('John', 'F. Kennedy', 40),
('Helen', 'Keller', 20),
('Serena', 'Williams', 23),
('Thomas', 'Edison', 23),
('Howard', 'Stern',45),
('Mark', 'Harmon', 45),
('Barack', 'Obama', 64),
('George', 'Bush', 19),
('Larry', 'David', 45),
('Freddie', 'Mercury', 5);

DELETE FROM employees WHERE employee_id = 5;

ALTER TABLE employees ADD COLUMN address varchar(255);
UPDATE employees SET ADDRESS = 'soi 3 moo 4' WHERE employee_id = 11;

SELECT COUNT(*) FROM employees;

SELECT firstname, lastname, age FROM employees WHERE age < 20;