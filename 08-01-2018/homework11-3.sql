INSERT INTO books (ISBN, title, price) VALUES
('578-2-12-345680-3', 'The Selection', 500),
('972-2-22-235680-2', 'JavaScript', 889),
('978-2-12-323480-3', 'The Sun and Her Flowers', 2432),
('938-2-12-345680-3', 'The Essential Instant Pot Cookbook', 123),
('238-2-12-344650-4', 'Lie to Me', 353),
('238-2-12-345682-3', 'Every Night Without You', 467),
('568-2-12-645682-4', 'The Last Mrs. Parrish', 354),
('698-2-12-745680-3', 'Mrs. Jeffries Reveals Her Art', 366),
('678-2-12-245680-5', 'Kane: I Am Alpha', 473),
('968-2-12-375680-3', 'The British Knight', 243);

SELECT * FROM books WHERE title LIKE '%Script%';

SELECT * FROM books WHERE title LIKE '%a%' limit 4;

INSERT INTO orders (employee_id, ISBN, price, amount) VALUES
(1, '972-2-22-235680-2', 889, 1),
(2, '968-2-12-375680-3', 243, 1),
(6, '972-2-22-235680-2', 889, 1),
(2, '578-2-12-345680-3', 500, 1),
(1, '698-2-12-745680-3', 366, 1),
(6, '972-2-22-235680-2', 2432, 1),
(7, '238-2-12-344650-4', 353, 1),
(8, '938-2-12-345680-3', 123, 1),
(3, '578-2-12-345680-3', 500, 1),
(2, '972-2-22-235680-2', 889, 1),
(3, '578-2-12-345680-3', 1500, 3),
(4, '938-2-12-345680-3', 123, 1),
(1, '972-2-22-235680-2', 889, 1),
(6, '238-2-12-345682-3', 467, 1),
(3, '972-2-22-235680-2', 889, 1),
(4, '568-2-12-645682-4', 354, 1),
(7, '972-2-22-235680-2', 889, 1),
(8, '972-2-22-235680-2', 889, 1),
(1, '238-2-12-344650-4', 353, 1),
(1, '678-2-12-245680-5', 473, 1);

SELECT SUM(price * amount) AS sold_amount FROM orders;

SELECT DISTINCT ISBN FROM orders;

SELECT SUM(price) AS total FROM orders;