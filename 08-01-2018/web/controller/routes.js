const views = require('koa-views');
const pug = require('pug');
const db = require('../model');
module.exports = function(app) {
    app.use(views('./views', {
        map: {pug: 'pug'},
        extension: 'pug'
      }));
    
    app.use(async (ctx, next) => {
        try {
            const employees = await db.employees.getEmployees();
            const books = await db.books.getBooks();
            
            const employeeCount = await db.employees.getEmployeeCount();
            const below20Employees = await db.employees.getAgeBelow(20);

            const ScriptBook = await db.books.getLike('%Script%');
            const aBook = await db.books.getLike('%a%', 4);

            const soldCount = await db.orders.getSoldCount();
            const soldBooks = await db.orders.getSoldBooks();
            const soldTotal = await db.orders.getSoldTotal();

            const titles = await db.titles.getTitles(true);
            const employee2Count = await db.employees2.getEmployeeCount();
            const lastNameCount = await db.employees2.getLastNameCount(true);

            await ctx.render('main', {
                employees,
                books,
                employeeCount,
                below20Employees,
                ScriptBook,
                aBook,
                soldCount,
                soldBooks,
                soldTotal,
                titles,
                employee2Count,
                lastNameCount
            });
        } catch (err) {
            ctx.status = 400;
            ctx.body = `Uh-oh: ${err.message}`;
        }
    });
}
