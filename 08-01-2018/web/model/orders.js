const Database = require('../lib/db');
const config = require('../config/db_book_store');

class Orders {
    constructor() {
        const {host, database, user, password} = config;
        this._db = new Database(host, database, user, password, 10);
    }
    getSoldBooks() {
        return this._db.execute('SELECT DISTINCT ISBN FROM orders');
    }
    async getSoldCount() {
        let soldCount = await this._db.execute('SELECT COUNT(amount) AS count FROM orders');
        return soldCount[0].count;
    }
    async getSoldTotal() {
        let total = await this._db.execute('SELECT SUM(price) AS total FROM orders');
        return total[0].total;
    }
}

module.exports = new Orders;