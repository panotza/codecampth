const Database = require('../lib/db');
const config = require('../config/db_book_store');

class Employees {
    constructor() {
        const {host, database, user, password} = config;
        this._db = new Database(host, database, user, password, 10);
    }
    getAgeBelow(age) {
        return this._db.execute(`SELECT employee_id, firstname, lastname FROM employees WHERE age < ${age}`);
    }
    getEmployees() {
        return this._db.execute('SELECT employee_id, firstname, lastname FROM employees');
    }
    async getEmployeeCount() {
        let employeeCount = await this._db.execute('SELECT COUNT(*) as count FROM employees');
        return employeeCount[0].count;
    }
}

module.exports = new Employees;