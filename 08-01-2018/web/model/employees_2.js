const Database = require('../lib/db');
const config = require('../config/db_employees');

class Employees {
    constructor() {
        const {host, database, user, password} = config;
        this._db = new Database(host, database, user, password, 10);
    }
    async getEmployeeCount() {
        let employeeCount = await this._db.execute(`SELECT COUNT(*) AS count, 
        (SELECT COUNT(*) AS male FROM employees WHERE gender = 'M') AS male,
        (SELECT COUNT(*) AS female FROM employees WHERE gender = 'F') AS female
        FROM employees`);
        return employeeCount[0];
    }
    async getLastNameCount(isDistinct) {
        let lastNameCount;
        if (isDistinct)
            lastNameCount = await this._db.execute('SELECT COUNT(DISTINCT last_name) as count FROM employees');
        else
            lastNameCount = await this._db.execute('SELECT COUNT(last_name) as count FROM employees');
        return lastNameCount[0].count;
    }
}

module.exports = new Employees;