const Database = require('../lib/db');
const config = require('../config/db_book_store');

class Books {
    constructor() {
        const {host, database, user, password} = config;
        this._db = new Database(host, database, user, password, 10);
    }
    getBooks() {
        return this._db.execute('SELECT ISBN, title, price, created_at FROM books');
    }
    getLike(str, limit) {
        if (limit)
            return this._db.execute(`SELECT ISBN, title, price, created_at FROM books WHERE title LIKE '${str}' limit ${limit}`);
        return this._db.execute(`SELECT ISBN, title, price, created_at FROM books WHERE title LIKE '${str}'`);
    }
}

module.exports = new Books;