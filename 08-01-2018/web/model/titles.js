const Database = require('../lib/db');
const config = require('../config/db_employees');

class Titles {
    constructor() {
        const {host, database, user, password} = config;
        this._db = new Database(host, database, user, password, 10);
    }
    getTitles(isDistinct) {
        if (isDistinct)
            return this._db.execute('SELECT DISTINCT title FROM titles');
        return this._db.execute('SELECT title FROM titles');
    }
}

module.exports = new Titles;