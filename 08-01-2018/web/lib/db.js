const mysql = require('mysql2');

class Database {
    constructor(host, database, user, password, connectionLimit) {
        this._host = host;
        this._database = database;
        this._user = user;
        this._password = password;
        this._connectionLimit = connectionLimit;
        this._pool = mysql.createPool({
                connectionLimit : this._connectionLimit,
                host: this._host,
                database: this._database,
                user: this._user,
                password: this._password
            });
    }
    execute(query) {
        return new Promise((reslove, reject) => {
            this._pool.getConnection(function(err, connection) {
                connection.query(query, (err, results) => {
                    if (err) reject(err);
                    // done with connection
                    connection.release();
                    // resolve
                    reslove(results);
                });
            });
        });
    }
}

module.exports = Database;