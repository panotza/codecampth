CREATE DATABASE book_store;

CREATE TABLE employees (
    employee_id INT NOT NULL AUTO_INCREMENT,
    firstname VARCHAR(100),
    lastname VARCHAR(100),
    age INT,
    created_at TIMESTAMP DEFAULT now(),
    PRIMARY KEY (employee_id)
);

CREATE TABLE books (
    ISBN VARCHAR(32) NOT NULL,
    title VARCHAR(100),
    price INT,
    created_at TIMESTAMP DEFAULT now(),
    PRIMARY KEY (ISBN)
);

CREATE TABLE orders (
    order_id INT NOT NULL AUTO_INCREMENT,
    employee_id INT NOT NULL,
    ISBN VARCHAR(32) NOT NULL,
    price INT,
    amount INT,
    sold_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (order_id),
    FOREIGN KEY (employee_id) REFERENCES employees(employee_id),
    FOREIGN KEY (ISBN) REFERENCES books(ISBN)
);