const views = require('koa-views');
const pug = require('pug');
const users = require('../model/homework10_1');

module.exports = function(app) {
    app.use(views('./views', {
        map: {pug: 'pug'},
        extension: 'pug'
      }));
    
    app.use(async (ctx, next) => {
        try {
            const rows = await users.getUsers();
            await ctx.render('user', {rows});
            await next();
        } catch (err) {
            ctx.status = 400;
            ctx.body = `Uh-oh: ${err.message}`;
        }
    });
}
