const fs = require('fs');

let data = fs.readFileSync('employee.txt', 'utf8');
let cleanedData = data.trim().replace(/\s+/g, '');
let peopleSalary = JSON.parse(cleanedData)

peopleSalary.forEach(people => {
    console.log(`${people.firstname} ${people.lastname}`);
});
