let fields = ['id','firstname','lastname','company','salary'];
let employees = [
    ['1001','Luke','Skywalker','Walt Disney','40000'],
    ['1002','Tony','Stark','Marvel','1000000'],
    ['1003','Somchai','Jaidee','Love2work','20000'],
    ['1004','Monkey D','Luffee','One Piece','9000000']
];

function createObject(keys, values) {
    let obj = [];
    employees.forEach((employee) => {
        let row = {};
        employee.forEach((val, i) => {
            row[keys[i]] = val;
        });
        obj.push(row);
    });
    return obj;
}

let table = createObject(fields, employees);
console.log(table);