import React, { Component } from 'react';
import { Form, Input, Button } from 'antd';
const FormItem = Form.Item;
const { TextArea } = Input;

class InputBox extends Component {

  constructor(props) {
    super(props);
    this.state = {
      value: '',
      sender: 0
    }
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(e) {
    this.setState({ value: e.target.value });
  }

  render() {
    const { onSave } = this.props;
    const { value } = this.state;
    return (
      <Form
        style={{ textAlign: 'center' }}
        layout="inline"
        onSubmit={(e) => {
          e.preventDefault();          
          const sender = this.state.sender ? 0 : 1;
          onSave(this.state.value, sender)
          this.setState({value : '', sender});
        }}
      >
        <FormItem>
          <TextArea
            style={{ width: '200px' }}
            value={value}
            onPressEnter={() => {}}
            onChange={this.handleChange}
            placeholder="write message here."
            autosize 
          />
        </FormItem>
        <FormItem>
          <Button type="primary" htmlType="submit">Send</Button>
        </FormItem>
      </Form>
    );
  }
}

export default InputBox;