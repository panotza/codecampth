import React from 'react';

const Message = ({position, text}) => {
  return (
    <p style={{textAlign: position}}>{text}</p>
  );
}

export default Message;