import React from 'react';
import { List, Avatar } from 'antd';
import InfiniteScroll from 'react-infinite-scroller';
import Message from './Message'

const ChatRoom = ({messages}) => {
  // const messageList = messages.map(m => 
  //   <Message 
  //     key={m.id} 
  //     text={m.text}
  //     position={m.sender ? 'right' : 'left'}
  //   />)
  return (
    <div style={{height: '500px', overflow: 'auto'}}>
      <InfiniteScroll
        useWindow={false}
        initialLoad={false}
        pageStart={0}
      >
        <List
        itemLayout="horizontal"
        dataSource={messages}
        bordered={false}
        renderItem={m => {
          if (m.sender) {
            return (
            <List.Item
              actions={
              [<List.Item.Meta
                avatar={<Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />}
                title={m.name}
                description={m.text}
              />]
              }
            >
            <br />
            </List.Item>
          );
          } else {
            return (
              <List.Item>
                <List.Item.Meta
                    avatar={<Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />}
                    title={m.name}
                    description={m.text}
                />
              </List.Item>
            );
          }
        }}
        />
      </InfiniteScroll>
    </div>
  );
}

export default ChatRoom;