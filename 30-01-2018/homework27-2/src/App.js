import React, { Component } from 'react';
import { Layout } from 'antd';
import './App.css';
import ChatRoom from './components/ChatRoom';
import InputBox from './components/InputBox';

const { Content } = Layout;

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      messages: [],
      nextId: 1    
    }
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit (text, sender) {
    const message = {
      id: this.state.nextId,
      sender,
      text,
      name: sender ? 'panot' : 'someone'
    }
    const messages = [...this.state.messages, message];
    this.setState({
      messages,
      nextId: this.state.nextId + 1
    });
  }

  render() {
    const { messages } = this.state;
    return (
      <Layout className="layout">
        <Content style={{ padding: '0 150px', height: '100%' }}>
          <div style={{ background: '#fff', padding: 24, minHeight: 280 }}>
            <ChatRoom messages={messages}/>
          </div>
          <InputBox onSave={this.handleSubmit}/>                  
        </Content>
      </Layout>
    );
  }
}

export default App;
