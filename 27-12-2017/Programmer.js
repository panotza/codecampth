const {Employee} = require('./Employee');

class Programmer extends Employee {
    constructor(fistName, lastName, salary, id, type) {
        super(fistName, lastName, salary);
        this._id = id;
        this.type = type;
    }

    createWebsite() {
        console.log('creating Website');
    }

    fixComputer() {
        console.log('fixing Computer');
    }

    installWindows() {
        console.log('install Windows');
    }
}

module.exports.Programmer = Programmer;