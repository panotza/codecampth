const {Employee} = require('./Employee');
const {CEO} = require('./CEO');
const {Programmer} = require('./Programmer');

const ceo = new CEO('somchai', 'sudlor', 30000);

async function createEmployees () {
    let employees = await ceo.loadEmployees();
    console.log(employees);
}

createEmployees();
