class MobilePhone {
    PhoneCall() {}
    SMS() {}
    InternetSurfing() {}
}

class SamsungPhone extends MobilePhone {
    UseGearVR() {}
    TransformToPC() {}
    GooglePlay() {}
}

class SamsungGalaxyNote8 extends SamsungPhone {
    UsePen() {}
}

class SamsungGalaxyS8 extends SamsungPhone {
}

class ApplePhone extends MobilePhone {
    AppleStore() {}
}

class IPhoneX extends ApplePhone {
    FaceID() {}
}
class IPhone8 extends ApplePhone {
    TouchID() {}
}