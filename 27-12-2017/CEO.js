const {Employee} = require('./Employee');
const {Programmer} = require('./Programmer');
const fs = require('fs');

class CEO extends Employee {
    constructor(firstName, lastName, salary) {
        super(firstName, lastName, salary);
        this.dressCode = 'suit';
    }
    getSalary(){  // simulate public method
        return super.getSalary()*2;
    };
    work (employee) {  // simulate public method
         this._fire(employee);
         this._hire(employee);
         this._seminar();
         this._golf();
    }
    increaseSalary(employee, newSalary) {
        if (!employee.setSalary(newSalary))
            console.log(`${employee.firstName}'s salary is less than before!!`);
        else
            console.log(`${employee.firstName}'s salary has been set to ${newSalary}`);
    }
    _fire(employee) {
        this.dressCode = 'tshirt';
        console.log(`${employee.firstName} has been fired! Dress with :${this.dressCode}`);
    }
    _hire(employee) {
        this.dressCode = 'tshirt';
        console.log(`${employee.firstName} has been hired back! Dress with :${this.dressCode}`);
    }
    _seminar() {
        this.dressCode = 'suit';
        console.log(`He is going to seminar Dress with :${this.dressCode}`);
    }
    _golf() { // simulate private method
        this.dressCode = 'golf_dress';
        console.log(`He goes to golf club to find a new connection. Dress with :${this.dressCode}`);
        
    };
    gossip(target, message) {
        console.log(`Hey ${target.firstName}, ${message}`);
    }
    async loadEmployees() {
        let data = await this._readFileP('homework1.json');
        this.employeesRaw = JSON.parse(data);
        this.employees = this.employeesRaw.map(employee => {
            let {id, firstname, lastname, salary} = employee;
            return new Programmer(firstname, lastname, salary, id);
        });
        return this.employees;
    }
    _readFileP(fileName) {
        return new Promise((resolve, reject) => {
            fs.readFile(fileName, 'utf8', (err, data) => err ? reject(err) : resolve(data));
        });
    }
}

module.exports.CEO = CEO;