const assert = require('assert');
const {Employee} = require('./Employee');
const {CEO} = require('./CEO');
const {Programmer} = require('./Programmer');
const sinon = require('sinon');
const fs = require('fs');

describe('Employee Class Test', function () {
    let somsri = new Employee("Somsri","Sudsuay",22000);
    describe('#setSalary()', function () {
        it('should return false if newSalary less than oldSalary', function() {
            assert.strictEqual(somsri.setSalary(5), false);
        });
        it('should return true if newSalary more than oldSalary', function() {
            assert.strictEqual(somsri.setSalary(99999), true);
        });
    });
    describe('#getSalary()', function () {
        it('should return 22000', function() {
            assert.strictEqual(somsri.getSalary(), 22000);
        });
    });
});

describe('CEO Class Test', function () {
    let somchai = new CEO("Somchai","Sudlor",30000);
    describe('#getSalary()', function () {
        it('should return 30000', function() {
            assert.strictEqual(somchai.getSalary(), 60000);
        });
    });
    describe('#work()', function () {
        it('should call _fire, _hire, _seminar and _golf method', function() {
            let stubFire = sinon.stub(somchai, '_fire');
            let stubHire = sinon.stub(somchai, '_hire');
            let stubSeminar = sinon.stub(somchai, '_seminar');
            let stubGolf = sinon.stub(somchai, '_golf');
            // call work
            somchai.work();
            // assert
            sinon.assert.calledOnce(stubFire);
            sinon.assert.calledOnce(stubHire);
            sinon.assert.calledOnce(stubSeminar);
            sinon.assert.calledOnce(stubGolf);
            // restore
            stubFire.restore();
            stubHire.restore();
            stubSeminar.restore();
            stubGolf.restore();
        });
    });
    describe('#loadEmployee()', function () {
        it('should call callback', function() {
            let stub = sinon.stub(fs, 'readFile');
            let callbackDummy = sinon.spy();

            stub.yields([
                {
                    "id": 1001,
                    "firstname": "Luke",
                    "lastname": "Skywalker",
                    "company": "Walt Disney",
                    "salary": 40000
                }]);

            somchai.loadEmployee(callbackDummy);
            sinon.assert.calledOnce(stub);
            sinon.assert.calledOnce(callbackDummy);
            stub.restore();
        });
    });
});