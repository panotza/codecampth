class Employee {
    constructor(firstName, lastName, salary) {
        this._salary = salary; // simulate private variable
        this.firstName = firstName;
        this.lastName = lastName;
    }
    setSalary(newSalary) { // simulate public method
        // return newSalary ถ้ามีเงินเดือนใหม่มีค่ามากกว่า this._salary
        // return false ถ้าเงินเดือนใหม่มีค่าน้อยกว่าเท่ากับ this._salary
        return (this.getSalary() > newSalary) ? false : true;
        
    }
    getSalary () {  // simulate public method
        return this._salary;
    };
    work(employee) {
        // leave blank for child class to be overidden
    }
    leaveForVacation(year, month, day) {

    }
}

module.exports.Employee = Employee;