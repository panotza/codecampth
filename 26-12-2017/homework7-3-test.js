const assert = require('assert');
const {cloneObject} = require('./homework7-3.js');

describe('#cloneObject()', function() {
    let input1 = [1,2,3],
        input2 = {a:1,b:2},
        input3 = [1,2,{a:1,b:2}],
        input4 = [1,2,{a:1,b:{c:3,d:4}}];
    it('should return new array or new object', function() {
        assert.notStrictEqual(cloneObject(input1), input1);
        assert.notStrictEqual(cloneObject(input2), input2);
        assert.notStrictEqual(cloneObject(input3)[2], input3[2]); //object in 2st dimension
        assert.notStrictEqual(cloneObject(input4)[2].b, input4[2].b); //object in 3nd dimension
     });
     it('should have same value as input', function() {
         assert.deepStrictEqual(cloneObject(input1), input1);
         assert.deepStrictEqual(cloneObject(input2), input2);
         assert.deepStrictEqual(cloneObject(input3), input3);
         assert.deepStrictEqual(cloneObject(input4), input4);
     });
     it('should have same key as input', function() {
         assert.deepEqual(Object.keys(cloneObject(input2)), Object.keys(input2));
         assert.deepEqual(Object.keys(cloneObject(input3)[2]), Object.keys(input3[2])); //object in 2st dimension
         assert.deepEqual(Object.keys(cloneObject(input4)[2].b), Object.keys(input4[2].b)); //object in 3nd dimension
     });
     it('should not modifiy input value when change output value', function() {
         let result1 = cloneObject(input1); //[1, 2, 3]
         result1[0] = 3;
         assert.notDeepEqual(result1, input1);

         let result2 = cloneObject(input2); //{a:1,b:2}
         result2.a = 4;
         assert.notDeepEqual(result2, input2);

         let result3 = cloneObject(input3); //[1,2,{a:1,b:2}]
         result3[2].a = 5;
         assert.notDeepEqual(result3, input3);

         let result4 = cloneObject(input4); //[1,2,{a:1,b:{c:3,d:4}}]
         result4[2].b.c = 5;
         assert.notDeepEqual(result4, input4);
     });
});