function cloneObject(original) {
    let newObj, value, key;
    newObj = Array.isArray(original) ? [] : {};
    for (key in original) {
        value = original[key];
        newObj[key] = (typeof value === "object") ? cloneObject(value) : value;
    }
    return newObj;
}

module.exports = {
    cloneObject
}