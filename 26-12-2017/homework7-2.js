const fs = require('fs');

function calculatePercentage(percentage, number) {
    return (percentage / 100) * number;
}

function calculateSalary(baseSalary, times, percentage) {
    let totalSalary = [baseSalary];
    for(let i = 1; i < times; i++) {
        let lastYearSalary = totalSalary[totalSalary.length-1];
        totalSalary.push(lastYearSalary + calculatePercentage(lastYearSalary, 10));
    }
    return totalSalary;
}

function addYearSalary(row) {
    row.yearSalary = row.salary * 12;
}

function addNextSalary(row) {
    row.nextSalary = calculateSalary(row.salary, 3, 10);
}

function addAdditionalFields(data) {
    let newData = data.map(employee => {
        addYearSalary(employee);
        addNextSalary(employee);
        return {...employee};
    });
    return newData;
}

let employees = fs.readFileSync('homework1.json', 'utf8');
    employees = JSON.parse(employees);
    let newEmployees = addAdditionalFields(employees)
    newEmployees[0].salary = 0;
    console.log(employees);
    console.log(newEmployees);