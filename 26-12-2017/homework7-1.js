const fs = require('fs');

function calculatePercentage(percentage, number) {
    return (percentage / 100) * number;
}

function calculateSalary(baseSalary, times, percentage) {
    let totalSalary = [baseSalary];
    for(let i = 1; i < times; i++) {
        let lastYearSalary = totalSalary[totalSalary.length-1];
        totalSalary.push(lastYearSalary + calculatePercentage(lastYearSalary, 10));
    }
    return totalSalary;
}

function addYearSalary(row) {
    row.yearSalary = row.salary * 12;
}

function addNextSalary(row) {
    row.nextSalary = calculateSalary(row.salary, 3, 10);
}

function addAdditionalFields(data) {
    data.forEach(employee => {
        addYearSalary(employee);
        addNextSalary(employee);
    });
}

let employees = fs.readFileSync('homework1.json', 'utf8');
    employees = JSON.parse(employees);
    addAdditionalFields(employees);
    console.log(employees);