import React, { Component } from 'react';
import { Menu, Icon, Input, Button } from 'antd';

const Search = Input.Search;

class TopNav extends Component {
  constructor(props) {
    super(props);

    this.state = {
      current: 'mail'
    }

    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(e) {
    console.log('click ', e);
    this.setState({
      current: e.key,
    });
  }

  render() {
    return (
      <Menu
        onClick={this.handleClick}
        selectedKeys={[this.state.current]}
        mode="horizontal"
      >
        <Menu.Item key="home">
          <i className="fa fa-home" aria-hidden="true" />Home
        </Menu.Item>
        <Menu.Item key="notifications">
          <i className="fa fa-bell-o" aria-hidden="true" />Notifications
        </Menu.Item>
        <Menu.Item key="messages">
          <i className="fa fa-envelope-o" aria-hidden="true" />Messages
        </Menu.Item>
        <li style={{ float: 'right' }}>
          <Search
            placeholder="input search text"
            onSearch={value => console.log(value)}
            style={{ width: 200 }}
          />
          <Button type="primary" shape="circle" icon="download" size="normal" />
          <Button type="primary" size="normal">Tweet</Button>
        </li>
        <li>
          <i className="fa fa-twitter" aria-hidden="true" />
        </li>
      </Menu>
    );
  }
}

export default TopNav;