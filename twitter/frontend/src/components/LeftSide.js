import React, { Component } from 'react';
import { Card } from 'antd';

class LeftSide extends Component {
  constructor (props) {
    super(props);
  }

  render () {
    return (
      <div style={{ background: '#ECECEC' }}>
        <Card title="Card title" bordered={false} style={{ width: '100%' }}>
          <p>Card content</p>
          <p>Card content</p>
          <p>Card content</p>
        </Card>
      </div>
    );
  }
}

export default LeftSide;