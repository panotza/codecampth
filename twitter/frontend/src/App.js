import React, { Component } from 'react';
import { Col, Row } from 'antd';
import TopNav from './components/TopNav';
import LeftSide from './components/LeftSide';
import RightSide from './components/RightSide';
import Center from './components/Center';
import 'font-awesome/css/font-awesome.css';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <TopNav />
        <Row style={{ padding: 25 }}>
          <Col span={6}>
            <div className="gutter-box">
              <LeftSide />
            </div>
          </Col>
          <Col span={12}>
            <div className="gutter-box">
              <Center />
            </div>
          </Col>
          <Col span={6}>
            <div className="gutter-box">
              <RightSide />
            </div>
          </Col>
        </Row>
      </div>
    );
  }
}

export default App;
