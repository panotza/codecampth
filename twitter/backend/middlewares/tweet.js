module.exports = {
    async validateTweet(ctx) {
        ctx.validateBody('text')
            .isString()
            .trim()
            .isLength(1, 140, 'you can tweet 140 chars');
        await next();
    }
}