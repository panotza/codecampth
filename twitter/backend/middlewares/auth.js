const repo = require('../repository');

module.exports = {
    async needLogin(ctx, next) {
        if (!ctx.session.userId) {
            ctx.throw(401, 'unauthorized')
        }
        await next();
    },
    async validateLogin(ctx, next) {
        ctx.validateBody('email')
            .isString()
            .trim()
            .isEmail('Invalid email format');
        ctx.validateBody('password')
            .required('Password required')
            .isString()
            .isLength(9, 100, 'Password must be 9-100 chars');
        await next();
    },
    async validateSignUp(ctx, next) {
        ctx.validateBody('email')
            .isString()
            .trim()
            .isEmail('Invalid email format');
        ctx.validateBody('username')
            .isString()
            .trim();
        ctx.validateBody('password')
            .required('Password required')
            .isString()
            .isLength(9, 100, 'Password must be 9-100 chars');
        ctx.validateBody('name')
            .required('name required')
            .trim()
            .isString()
            .isLength(4, 40, 'Password must be 4-40 chars');
        await next();
    }
}