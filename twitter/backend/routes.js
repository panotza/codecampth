const {
    user, 
    auth, 
    upload, 
    tweet, 
    directMessage, 
    notification
} = require('./controllers');
const m = require('./middlewares');

module.exports = function routes(app) {
    app.prefix('/api');
    
    app
    //Auth
    .post('/auth/signup', m.auth.validateSignUp, auth.signup) //done
    .post('/auth/signin', m.auth.validateLogin, auth.signin) //done
    .get('/auth/signout', auth.signout) //done
    .post('/auth/verify', auth.verify)
    
    .use(m.auth.needLogin) // user must logged in before go to these routes

    //Upload
    .post('/upload', upload.image) //done

    //User
    .patch('/user/:id', user.update) // done
    .put('/user/:id/follow', user.follow) // done
    .delete('/user/:id/follow', user.unfollow) // done
    .get('/user/:id/follow', user.followingList) //done
    .get('/user/:id/followed', user.followerList) //done

    //Twitter
    .get('/tweet', tweet.list) // done
    .post('/tweet', tweet.store) // done
    .put('/tweet/:id/like', tweet.like) //done
    .delete('/tweet/:id/like', tweet.unlike) //done
    .post('/tweet/:id/retweet', tweet.retweet) //done
    .put('/tweet/:id/vote/:voteId', tweet.vote) //done
    .post('/tweet/:id/reply', tweet.reply) //done

    //Notification
    .get('/notification', notification.list) //done

    //Direct Message
    .get('/message', directMessage.list) //done
    .get('/message/:id', directMessage.userMessages) //done
    .post('/message/:id', directMessage.send) //done
};