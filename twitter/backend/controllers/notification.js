const {notification, user, tweet} = require('../repository');

module.exports = {
    async list (ctx) {
        ctx.body = await notification.findAll(ctx.db, ctx.session.userId);
    },
    async handleReply({db, request, session, params}) {
        const actionUser = await user.findById(db, session.userId, session.userId);
        const reply = await tweet.findReplyById(db, params.id);
        const data = {
            db,
            title: `${actionUser.name} has replied your tweet`,
            content: reply.text,
            owner: reply.userId,
            actionUser
        }
        notification.store(data);
    },
    async handleLike({db, request, session, params}) {
        const actionUser = await user.findById(db, session.userId, session.userId);
        const t = await tweet.findById(db, params.id, session.userId);
        const data = {
            db,
            title: `${actionUser.name} liked your tweet`,
            content: '',
            owner: t.user.id,
            actionUser
        }
        notification.store(data);
    },
    async handleRetweet({db, request, session, params}) {
        const actionUser = await user.findById(db, session.userId, session.userId);
        const t = await tweet.findById(db, params.id, session.userId);
        const data = {
            db,
            title: `${actionUser.name} retweet your tweet`,
            content: request.body.text,
            owner: t.user.id,
            actionUser
        }
        notification.store(data);
    },
    async handleVote({db, request, session, params}) {
        const actionUser = await user.findById(db, session.userId, session.userId);
        const t = await tweet.findById(db, params.id, session.userId);
        const data = {
            db,
            title: `${actionUser.name} answered your poll`,
            content: '',
            owner: t.user.id,
            actionUser
        }
        notification.store(data);
    },
    async handleFollow({db, request, session, params}) {
        const actionUser = await user.findById(db, session.userId, session.userId);
        const followee = await user.findById(db, params.id, session.userId);
        const data = {
            db,
            title: `${actionUser.name} start following you`,
            content: '',
            owner: followee.id,
            actionUser
        }
        notification.store(data);
    },
    async handleSendDm({db, request, session, params}) {
        const actionUser = await user.findById(db, session.userId, session.userId);
        const receiver = await user.findById(db, params.id, session.userId);
        const data = {
            db,
            title: `${actionUser.name} has send you a message`,
            content: request.body.text,
            owner: receiver.id,
            actionUser
        }
        notification.store(data);
    }
}