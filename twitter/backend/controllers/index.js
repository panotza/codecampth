module.exports = {
    auth: require('./auth'),
    tweet: require('./tweet'),
    user: require('./user'),
    upload: require('./upload'),
    directMessage: require('./directMessage'),
    notification: require('./notification')
}