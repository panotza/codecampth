const repo = require('../repository');
const notification = require('./notification');

module.exports = {
    async update (ctx) {
        await repo.user.update(ctx, ctx.request.body, ctx.params.id);
        ctx.body = {};
    },
    async follow (ctx) {
        await repo.user.follow(ctx.db, ctx.session.userId, ctx.params.id);
        notification.handleFollow(ctx);
        ctx.body = {};
    },
    async unfollow (ctx) {
        await repo.user.unfollow(ctx.db, ctx.session.userId, ctx.params.id);
        ctx.body = {};
    },
    async followingList (ctx) {
        ctx.body = await repo.user.followingList(ctx.db, ctx.params.id, ctx.session.userId);
    },
    async followerList (ctx) {
        ctx.body = await repo.user.followerList(ctx.db, ctx.params.id, ctx.session.userId);
    }
}