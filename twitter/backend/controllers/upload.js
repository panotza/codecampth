const fs = require('fs');
const join = require('path').join;
const jimp = require("jimp");
const multer = require('koa-multer')
const upload = multer({ dest: join('tmp') })
const repo = require('../repository');

async function saveImage (original, filename, opts) {
    try {
        let image = await jimp.read(original);
        image.resize(opts.x, opts.y)
            .quality(80)
            .write(filename);
    } finally {
        fs.unlink(original, () => {});
    }
}

module.exports = {
    async image (ctx, next) {
        let file, savePath, opts = {};
        await upload.array('file', 5)(ctx)
        switch (ctx.req.body.type) {
            case 'tweet_photo':
                opts.x = 500;
                opts.y = 600;

                if (!+ctx.req.body.tweet_id) ctx.throw(400, 'wrong tweet id');
                const tweet = await repo.tweet.findById(ctx.db, ctx.req.body.tweet_id, ctx.session.userId);

                if (!tweet.type || tweet.type !== 'photo') {
                    ctx.throw(400, 'wrong tweet');
                }

                for (file of ctx.req.files) {
                    savePath = join('images', 'tweet_photos', file.originalname);
                    await saveImage(file.path, savePath, opts);
                }
                await repo.tweetPhoto.store(ctx.db, ctx.req.body.tweet_id, savePath);
                break;
            case 'profile_photo':
                opts.x = 150;
                opts.y = 150;
                file = ctx.req.files[0];
                savePath = join('images', 'profile_photos', file.originalname);
                await saveImage(file.path, savePath, opts);
                await repo.user.storeProfilePhoto(ctx.db, ctx.session.userId, savePath);
                break;
            case 'cover_photo':
                opts.x = 400;
                opts.y = 900;
                file = ctx.req.files[0];
                savePath = join('images', 'cover_photos', file.originalname);
                console.log(savePath);
                await saveImage(file.path, savePath, opts);
                await repo.user.storeCoverPhoto(ctx.db, ctx.session.userId, savePath);
                break;
            default:
                ctx.throw(400, 'wrong photo type');
        }
        ctx.body = {};
    }
}