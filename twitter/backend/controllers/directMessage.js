const repo = require('../repository');
const notification = require('./notification');

module.exports = {
    async list (ctx) {
        ctx.body = await repo.directMessage.list(ctx.db, ctx.session.userId);
    },
    async userMessages (ctx) {
        ctx.body = await repo.directMessage.listByUserId(ctx.db, ctx.params.id, ctx.session.userId);
    },
    async send (ctx) {
        await repo.directMessage.store(ctx.db, ctx.session.userId, ctx.params.id, ctx.request.body.text);
        notification.handleSendDm(ctx);
        ctx.body = {};
    },
}