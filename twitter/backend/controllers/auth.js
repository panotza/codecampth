const repo = require('../repository');
const bcrypt = require('bcrypt');

module.exports = {
    async signup (ctx) {
        let { password } = ctx.request.body;
        password = await bcrypt.hash(password, 10);
        await repo.user.store(ctx, {...ctx.request.body, password})
        ctx.body = {};
    },
    async signin (ctx) {
        let { email, password } = ctx.request.body;
        const user = await repo.user.findByEmail(ctx.db, email);
        if (!user) {
            ctx.throw(401, 'user not found');
        }
        const isMatch = await bcrypt.compare(password, user.password);
        if (!isMatch) {
            ctx.throw(401, 'wrong password');
        }
        ctx.session.userId = user.id;
        ctx.body = {};
    },
    async signout (ctx) {
        ctx.session = null;
        ctx.body = {};
    },
    async verify (ctx) {
        ctx.body = 'token..';
    }
}