const repo = require('../repository');
const notification = require('./notification');

module.exports = {
    async list (ctx) {
        const { tweets } = await repo.tweet.findByUsername(ctx.db, ctx.query, ctx.session.userId);
        for (tweet of tweets) {
            tweet.reply = await repo.tweet.findReplyById(ctx.db, tweet.id)
            tweet.user.isFollowing = await repo.user.isFollowing(ctx.db, ctx.session.userId, tweet.user.id)
        }
        ctx.body = { tweets };
    },
    async store (ctx) {
        const id = await repo.tweet.store(ctx.db, ctx.request.body);
        if (id <=0) {
            ctx.throw(400, 'cannot tweet.');
        }
        ctx.body = await repo.tweet.findById(ctx.db, id, ctx.session.userId);
    },
    async like (ctx) {
        await repo.tweet.like(ctx.db, ctx.session.userId, ctx.params.id);
        notification.handleLike(ctx);
        ctx.body = {};
    },
    async unlike (ctx) {
        await repo.tweet.unlike(ctx.db, ctx.session.userId, ctx.params.id);
        ctx.body = {};
    },
    async retweet (ctx) {
        await repo.tweet.retweet(ctx.db, ctx.session.userId, ctx.params.id, ctx.request.body);
        notification.handleRetweet(ctx);
        ctx.body = {};
    },
    async vote (ctx) {
        await repo.tweet.vote(ctx.db, ctx.session.userId, ctx.params.id, ctx.params.voteId);
        notification.handleVote(ctx);
        ctx.body = {};
    },
    async reply (ctx) {
        await repo.tweet.reply(ctx.db, ctx.session.userId, ctx.params.id, ctx.request.body);
        notification.handleReply(ctx);
        ctx.body = {};
    }
}