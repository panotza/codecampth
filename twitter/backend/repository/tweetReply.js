const squel = require('squel');
const user = require('./user')
module.exports = {
    async findByTweetId (db, tweetId, currentUserId) {
        const q = squel.select()
            .field('r.id')
            .field('r.reply_text')
            .field('u.id', 'user_id')
            .field('u.username')
            .field('u.profile_photo')
            .field('u.cover_photo')
            .field('u.bio')
            .from('replies', 'r')
            .left_join('users', 'u', 'r.user_id = u.id')
            .where('r.tweet_id = ?', tweetId)
            .toParam();
        const [rows] = await db.execute(q.text, q.values);
        return await Promise.all(rows.map(async reply => {
            return {
                id: reply.id,
                text: reply.reply_text,
                user: {
                    id: reply.user_id,
                    username: reply.username,
                    profilePhoto: reply.profile_photo,
                    coverPhoto: reply.cover_photo,
                    bio: reply.bio,
                    isFollow: await user.isFollowing(db, currentUserId, reply.user_id)
                }
            }
        }));
    }
}