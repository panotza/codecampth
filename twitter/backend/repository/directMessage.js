const squel = require('squel');
const user = require('./user');

module.exports = {
    async store (db, userId, receiverId, text) {
        const q = squel.insert()
            .into('direct_messages')
            .set('sender_id', +userId)
            .set('receiver_id', +receiverId)
            .set('dm_text', text)
            .toParam();
        const [result] = await db.execute(q.text, q.values);
        return result.insertId;
    },
    async list (db, sessionUserId) {
        const q = squel.select()
            .field('dm.dm_text')
            .field('dm.created_at')
            .field('dm.is_read')
            .field('u.id')
            .field('u.username')
            .field('u.profile_photo')
            .field('u.cover_photo')
            .field('u.bio')
            .from('direct_messages', 'dm')
            .left_join('users', 'u', 'dm.sender_id = u.id')
            .toString();
        const [rows] = await db.execute(q);
        return {
            messages: await Promise.all(rows.map(async m => {
                return {
                    text: m.dm_text,
                    createdAt: m.created_at,
                    read: Boolean(m.is_read),
                    user: await user.findById(db, m.id, sessionUserId)
                }
            }))
        };
    },
    async listByUserId (db, userId, sessionUserId) {
        const q = squel.select()
            .field('dm_text')
            .field('created_at')
            .field('is_read')
            .from('direct_messages')
            .where('sender_id = ?', userId)
            .toParam();
        const [rows] = await db.execute(q.text, q.values);
        return {
            user: await user.findById(db, userId, sessionUserId),
            messages: rows.map(m => {
                return {
                    text: m.dm_text,
                    createdAt: m.created_at,
                    read: Boolean(m.is_read)
                }
            })
        };
    }
}