const squel = require('squel');

function createUser (user) {
    return {
        name: user.name,
        profilePhoto: user.profile_photo,
        coverPhoto: user.cover_photo,
        location: user.location,
        bio: user.bio,
        theme: user.theme,
        birthDateD: user.birthday,
        birthDateM: user.birthday,
        birthDateY: user.birthday,
        showBirthDateY: user.show_birthdate_y ? true : false,
        showBirthDateDM: user.show_birthdate_dm ? true : false
    };
}

module.exports = {
    async store (ctx, user) {
        const q = squel.insert()
            .into('users')
            .set('email', user.email)
            .set('username', user.username)
            .set('password', user.password)
            .set('name', user.name)
            .toParam();
        const [result] = await ctx.db.execute(q.text, q.values);
        return result.insertId;
    },
    async update (ctx, user, id) {
        const q = squel.update()
            .table('users')
            .set('name', user.name)
            .set('profile_photo', user.profilePhoto)
            .set('cover_photo', user.coverPhoto)
            .set('location', user.location)
            .set('bio', user.bio)
            .set('theme', user.theme)
            .set('birth_date_d', +user.birth_date_d || null)
            .set('birth_date_m', +user.birth_date_m || null)
            .set('birth_date_y', +user.birth_date_y || null)
            .set('show_birthdate_y', user.showBirthDateY == 'true' ? 1 : 0)
            .set('show_birthdate_dm', user.showBirthDateDM == 'true' ? 1 : 0)
            .where("id = ?", id)
            .toParam();
        const result = await ctx.db.execute(q.text, q.values);
        return result.affectedRows;
    },
    async remove (db, id) {
        const q = squel.delete()
            .from("users")
            .where("id = ?", id)
            .toString()
        const [result] = await db.execute(q);
        return result.affectedRows;
    },
    async findById (db, id, currentUserId) {
        const q = squel.select()
            .from('users')
            .field('id')
            .field('username')
            .field('name')
            .field('profile_photo')
            .field('bio')
            .where("id = ?", id)
            .toParam();
        const [rows] = await db.execute(q.text, q.values);
        const user = rows[0];
        if (!user) {
            return {};
        }
        return {
            id: user.id,
            username: user.username,
            name: user.name,
            profilePhoto: user.profile_photo,
            bio: user.bio,
            isFollowing: await this.isFollowing(db, currentUserId, id)
        };
    },
    async findByUsername (db, username) {
        const q = squel.select()
            .from('users')
            .field('name')
            .field('profile_photo')
            .field('cover_photo')
            .field('location')
            .field('bio')
            .field('theme')
            .field('birth_date_d')
            .field('birth_date_m')
            .field('birth_date_y')
            .field('show_birthdate_y')
            .field('show_birthdate_dm')
            .where("username = ?", id)
            .toParam();
        const [rows] = await db.execute(q.text, q.values);
        return createUser(rows[0]);
    },
    async findByEmail (db, email) {
        const q = squel.select()
            .from('users')
            .field('id')
            .field('password')
            .where("email = ?", email)
            .toParam();
        const [rows] = await db.execute(q.text, q.values);
        return rows[0];
    },
    async checkUsernameIfExists (db, username) {
        const q = squel.select()
            .from('users')
            .field('id')
            .where("username = ?", username)
            .toParam();
            const [rows] = await db.execute(q.text, q.values);
            return rows[0] ? true : false;
    },
    async checkEmailIfExists (db, email) {
        const q = squel.select()
            .from('users')
            .field('id')
            .where("email = ?", email)
            .toParam();
        const [rows] = await db.execute(q.text, q.values);
        return rows[0] ? true : false;
    },
    async follow (db, userId, followeeId) {
        const q = squel.insert()
            .into('follows')
            .set('follower_id', userId)
            .set('followee_id', +followeeId)
            .toParam();
        const [result] = await db.execute(q.text, q.values);
        return result.affectedRows;
    },
    async unfollow (db, userId, followeeId) {
        const q = squel.delete()
            .from("follows")
            .where("follower_id = ? AND followee_id = ?", userId, +followeeId)
            .toParam();
        const [result] = await db.execute(q.text, q.values);
        return result.affectedRows;
    },
    async isFollowing (db, followerId, followeeId) {
        const q = squel.select()
            .from('follows')
            .field('COUNT(*)', 'is_following')
            .where('follower_id = ? AND followee_id = ?', followerId, followeeId)
            .toParam();
        const [rows] = await db.execute(q.text, q.values);
        if (!rows[0]) {
            return false;
        }
        return rows[0].is_following ? true : false;
    },
    async followingList (db, userId, currentUserId) { //แสดงรายชื่อที่ user id follow อยู่
        const q = squel.select()
            .from('follows', 'f')
            .field('u.id')
            .field('u.username')
            .field('u.profile_photo')
            .field('u.cover_photo')
            .field('u.bio')
            .join('users', 'u', 'f.followee_id = u.id')
            .where('f.follower_id = ?', +userId)
            .toParam();
        const [rows] = await db.execute(q.text, q.values);
        return {
            users: await Promise.all(rows.map(async user => {
                return {
                    id: user.id,
                    username: user.username,
                    profilePhoto: user.profile_photo,
                    coverPhoto: user.cover_photo,
                    bio: user.bio,
                    isFollowing: await this.isFollowing(db, currentUserId, user.id)
                }
            }))
        };
    },
    async followerList (db, userId, currentUserId) { //แสดงรายชื่อคนที่ follow user id
        const q = squel.select()
            .from('follows', 'f')
            .field('u.id')
            .field('u.username')
            .field('u.profile_photo')
            .field('u.cover_photo')
            .field('u.bio')
            .join('users', 'u', 'f.follower_id = u.id')
            .where('f.followee_id = ?', +userId)
            .toParam();
        const [rows] = await db.execute(q.text, q.values);
        return {
            users: await Promise.all(rows.map(async user => {
                return {
                    id: user.id,
                    username: user.username,
                    profilePhoto: user.profile_photo,
                    coverPhoto: user.cover_photo,
                    bio: user.bio,
                    isFollowing: await this.isFollowing(db, currentUserId, user.id)
                }
            }))
        };
    },
    async storeProfilePhoto (db, userId, imageUrl) {
        const q = squel.update()
            .table('users')
            .set('profile_photo', imageUrl)
            .where('id = ?', userId)
            .toParam();
        const [result] = await db.execute(q.text, q.values);
        return result.affectedRows;
    },
    async storeCoverPhoto (db, userId, imageUrl) {
        const q = squel.update()
            .table('users')
            .set('cover_photo', imageUrl)
            .where('id = ?', userId)
            .toParam();
        const [result] = await db.execute(q.text, q.values);
        return result.affectedRows;
    }
}