const squel = require('squel');
const photo = require('./tweetPhoto');
const answer = require('./tweetAnswer');
const reply = require('./tweetReply');
const user = require('./user');
const moment = require('moment');

function createTweet(row) {
    return {
        id: row.id,
        text: row.tweet_text,
        type: row.tweet_type,
        photos: row.tweet_photos ? JSON.parse(row.tweet_photos) : [],
        hashtags: row.hashtags ? row.hashtags.split(',') : [],
        user: {
            id: row.user_id,
            username: row.username,
            name: row.name,
            profilePhoto: row.profile_photo,
            coverPhoto: row.cover_photo,
            bio: row.bio,
        },
        liked: row.liked,
        retweet: row.retweet,
        poll: row.answers ? JSON.parse(row.answers) : []
    }
}

function findHashtag(text) {
    const regex = /(^|\B)#(?![0-9_]+\b)([a-zA-Z0-9_]{1,30})(\b|\r)/g;
    let hashtags = new Set();
    let m;
    while ((m = regex.exec(text)) !== null) {
        // This is necessary to avoid infinite loops with zero-width matches
        if (m.index === regex.lastIndex) {
            regex.lastIndex++;
        }
        
        // The result can be accessed through the `m`-variable.
        m.forEach((match, groupIndex) => {
            if (groupIndex === 0) hashtags.add(match);
        });
    }
    return hashtags;
}

module.exports = {
    async store (db, tweet) {  // need try catch ? and transaction ?
        // check tweet type
        let type;
        if (tweet.photos) {
            type = 'photo';
        } else if (tweet.poll) {
            type = 'poll';
        } else {
            type = 'text';
        }
        // insert tweet
        const q = squel.insert()
            .into("tweets")
            .set('tweet_text', tweet.text)
            .set('tweet_type', type)
            .set('end_date', 
            (tweet.poll && tweet.poll.end) 
            ? moment(tweet.poll.end).utc().format('YYYY-MM-DD hh:mm:ss') // should be like 2018-01-17 00:41:16
            : null)
            .set('user_id', +tweet.user_id)
            .toParam();
        const [result] = await db.execute(q.text, q.values);
        const tweetId = result.insertId;

        // return if cannot create tweet
        if (tweetId <= 0) {
            return;
        }
        await this.storeHashtag(db, tweetId, tweet.text);
        // insert photo
        if (type === 'photo') {
            let r = await Promise.all(tweet.photos.map(async url => 
                await this.store_photo(db, tweetId, url)
            ));
            r = r.every(v => v > 0); // need implement
        }
        // insert answer
        if (type === 'poll') {
            let r = await Promise.all(tweet.poll.votes.map(async title => 
                await this.store_answer(db, tweetId, title)
            ));
            r = r.every(v => v > 0); // need implement
        }
        return tweetId;
    },
    async storeHashtag (db, tweetId, text) {
        let hashTagData = Array.from(findHashtag(text)).map(v => { 
            return {tweet_id: tweetId, hashtag_name: v}
        });
        const q = squel.insert()
            .into("tweet_hashtags")
            .setFieldsRows(hashTagData)
            .toParam();
        const [result] = await db.execute(q.text, q.values);
        return result.affectedRows;
    },
    async store_photo (db, tweetId, url) {
        const q = squel.insert()
            .into("tweet_photos")
            .set('image_url', url)
            .set('tweet_id', +tweetId)
            .toParam();
        const [result] = await db.execute(q.text, q.values);
        return result.insertId;
    },
    async store_answer (db, tweetId, title) {
        const q = squel.insert()
            .into("tweet_answers")
            .set('title', title)
            .set('tweet_id', +tweetId)
            .toParam();
        const [result] = await db.execute(q.text, q.values);
        return result.insertId;
    },
    async like (db, currentUserId, tweetId) {
        const q = squel.insert()
            .into("likes")
            .set('user_id', currentUserId)
            .set('tweet_id', +tweetId)       
            .toParam();
        const [result] = await db.execute(q.text, q.values);
        return result.affectedRows;
    },
    async unlike (db, currentUserId, tweetId) {
        const q = squel.delete()
            .from("likes")
            .where("user_id = ? AND tweet_id = ?", currentUserId, tweetId)
            .toParam();
        const [result] = await db.execute(q.text, q.values);
        return result.affectedRows;
    }, 
    async retweet (db, currentUserId, tweetId, retweet) {
        const q = squel.insert()
            .into("retweets")
            .set('retweet_text', retweet.text)
            .set('user_id', currentUserId)
            .set('tweet_id', +tweetId)            
            .toParam();
        const [result] = await db.execute(q.text, q.values);
        return result.affectedRows;
    },
    async reply (db, currentUserId, tweetId, reply) {
        const q = squel.insert()
            .into("replies")
            .set('reply_text', reply.text)
            .set('user_id', currentUserId)
            .set('tweet_id', +tweetId)            
            .toParam();
        const [result] = await db.execute(q.text, q.values);
        return result.insertId;
    },
    async vote (db, currentUserId, tweetId, answerId) {
        await this.removeVote(db, currentUserId, tweetId); // remove the old vote
        const q = squel.insert()
            .into("tweet_voting_history")
            .set('answer_id', +answerId)
            .set('user_id', currentUserId)
            .set('tweet_id', +tweetId)            
            .toParam();
        const [result] = await db.execute(q.text, q.values);
        return result.affectedRows;
    },
    async removeVote (db, userId, tweetId) {
        const q = squel.delete()
            .from("tweet_voting_history")
            .where("user_id = ? AND tweet_id = ?", userId, tweetId)
            .toParam();
        const [result] = await db.execute(q.text, q.values);
        return result.affectedRows;
    },
    async findById (db, id, currentUserId) {
        const q = squel.select()
            .field('id')
            .field('tweet_text')
            .field('tweet_type')
            .field('created_at')
            .field('user_id')
            .field('username')
            .field('name')
            .field('profile_photo')
            .field('cover_photo')
            .field('bio')
            .field('tweet_photos')
            .field('answers')
            .field('hashtags')
            .field('retweet')
            .field('liked')
            .from('denormalize_tweets', 't')
            .where('id = ?', id)
            .toParam();
        const [rows] = await db.execute(q.text, q.values);
        if (!rows[0]) return false;
        return createTweet(rows[0]);
    },
    async findByUsername (db, query, currentUserId) {
        if (!query.user) return {tweets: []};
        const opts = {
            user: query.user,
            start: query.start || '1970-01-01 00:00:01',
            limit: query.limit || 100,
            like: query.like || 'false'
        };
        opts.start = moment(opts.start).utc().format('YYYY-MM-DD hh:mm:ss');
        let q = squel.select()
            .field('id')
            .field('tweet_text')
            .field('tweet_type')
            .field('created_at')
            .field('user_id')
            .field('username')
            .field('name')
            .field('profile_photo')
            .field('cover_photo')
            .field('bio')
            .field('tweet_photos')
            .field('answers')
            .field('hashtags')
            .field('retweet')
            .field('liked')
            .from('denormalize_tweets', 't');
        if (opts.like.toLowerCase() === 'true') {
            q = q.where('id IN ?', 
                squel.select()
                .field('tweet_id')
                .from('likes')
                .where('user_id = ?', currentUserId));
        } else {
            q = q.where('username = ? AND created_at > ?', opts.user, opts.start);
        }
            q = q.offset(0).limit(+opts.limit).toParam();
        console.log(q);
        const [rows] = await db.execute(q.text, q.values);
        return {
            tweets: rows.map(createTweet)
        }
    },
    async removeVote (db, userId, tweetId) {
        const q = squel.delete()
            .from("tweet_voting_history")
            .where("user_id = ? AND tweet_id = ?", userId, tweetId)
            .toParam();
        const [result] = await db.execute(q.text, q.values);
        return result.affectedRows;
    },
    async findReplyById (db, id) {
        const q = squel.select()
            .field('id')
            .field('reply_text')
            .field('user_id')
            .field('username')
            .field('name')
            .field('profile_photo')
            .field('cover_photo')
            .field('bio')
            .from('denormalize_replies')
            .where('id = ?', id)
            .toParam();
        const [rows] = await db.execute(q.text , q.values);
        return rows.map(row => {
            return {
                id: row.id,
                text: row.reply_text,
                user: {
                    id: row.user_id,
                    username: row.username,
                    name: row.name,
                    profilePhoto: row.profile_photo,
                    coverPhoto: row.cover_photo,
                    bio: row.bio,
                    isFollow: false //todo
                }
            }
        });
    }
}