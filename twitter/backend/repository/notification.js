const squel = require('squel');

module.exports = {
    async store (data) {
        const q = squel.insert()
            .into('notifications')
            .set('title', data.title)
            .set('content', data.content)
            .set('user_id', data.owner)
            .toParam();
        const [result] = await data.db.execute(q.text, q.values);
        return result.insertId;
    },
    async findAll (db, currentUserId) {
        const q = squel.select()
            .from('notifications')
            .field('id')
            .field('title')
            .field('content')
            .field('is_read')
            .field('created_at')
            .where("user_id = ?", currentUserId)
            .toParam();
        const [rows] = await db.execute(q.text, q.values);
        return {
            notifications: rows.map(n => {
                return {
                    id: n.id,
                    title: n.title,
                    text: n.content,
                    photo: n.photo ? n.photo : undefined,
                    read: n.is_read ? true : false,
                    createdAt: n.create_at
                }
            })
        } 
    }
}