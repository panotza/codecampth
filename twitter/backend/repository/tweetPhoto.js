const squel = require('squel');

module.exports = {
    async store (db, tweetId, imageUrl) {
        const q = squel.insert()
            .into('tweet_photos')
            .set('tweet_id', +tweetId)
            .set('image_url', imageUrl)
            .toParam();
        const [result] = await db.execute(q.text, q.values);
        return result.affectedRows;
    },
    async findByTweetId (db, tweetId) {
        const q = squel.select()
            .field('id')
            .field('image_url', 'imageUrl')
            .from('tweet_photos')
            .where('tweet_id = ?', tweetId)
            .toParam();
        const [rows] = await db.execute(q.text, q.values);
        return rows.map(p => p.imageUrl);
    }
}