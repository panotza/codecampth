module.exports = {
    user: require('./user'),
    tweet: require('./tweet'),
    tweetPhoto: require('./tweetPhoto'),
    directMessage: require('./directMessage'),
    notification: require('./notification')
}