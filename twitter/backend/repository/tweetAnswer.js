const squel = require('squel');

function createAnswer (answer) {
    return {
        id: answer.id,
        text: answer.title,
        voted: answer.voted
    }
}

module.exports = {
    async findByTweetId (db, tweetId) {
        const q = squel.select()
            .field('ta.id')
            .field('ta.title')
            .field('COALESCE(vote.count, 0)', 'voted')
            .from('tweet_answers', 'ta')
            .left_join(
                squel.select()
                .field('answer_id')
                .field('COUNT(*)', 'count')
                .from('tweet_voting_history')
                .group('answer_id')
                , 'vote', 'ta.id = vote.answer_id')
            .where('ta.tweet_id = ?', tweetId)
            .toParam();
        const [rows] = await db.execute(q.text, q.values);
        return rows.map(createAnswer);
    }
}