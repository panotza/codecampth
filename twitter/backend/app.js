const Koa = require('koa');
const router = require('koa-router')();
const bodyParser = require('koa-bodyparser');
const session = require('koa-session');
const error = require('koa-json-error');
const bouncer = require('koa-bouncer');
const mysql = require('mysql2/promise');

const routes = require('./routes');
const config = require('./config/db');

const pool = mysql.createPool(config);
const app = new Koa();
app.keys = ['Teal Mercury Newfoundland'];

const sessionStore = {}
const sessionConfig = {
  key: 'sess',
  maxAge: 3600 * 1000,
  httpOnly: true,
  store: {
    get (key, maxAge, { rolling }) {
      return sessionStore[key];
    },
    set (key, sess, maxAge, { rolling }) {
      sessionStore[key] = sess;
    },
    destroy (key) {
      delete sessionStore[key];
    }
  }
}

function formatError(err) {
  return {
      status: err.status,
      message: err.message,
  }
}

// setup routes
routes(router);

app
  .use(error(formatError))
  .use(logger)
  .use(connectPool)
  .use(bouncer.middleware())
  .use(session(sessionConfig, app))
  .use(bodyParser())
  .use(router.routes())
  .use(router.allowedMethods())
  .listen(3000);

async function connectPool (ctx, next) {
  ctx.db = await pool.getConnection();
  try {
    await next();
  //} catch (err) {
  //  ctx.throw(400, err);
  } finally {
    ctx.db.release();
    console.log('db: connection released');
  }
}

async function logger (ctx, next) {
  console.log(`logger: ${ctx.method} ${ctx.path}`)
  await next();
}