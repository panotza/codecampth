DROP DATABASE IF EXISTS twitter;
CREATE DATABASE IF NOT EXISTS twitter;
USE twitter;

CREATE TABLE users (
    id INT PRIMARY KEY AUTO_INCREMENT,
    email VARCHAR(255) NOT NULL UNIQUE,
    username VARCHAR(30) NOT NULL UNIQUE,
    password VARCHAR(255) NOT NULL,
    name VARCHAR(40) NOT NULL DEFAULT '',
    profile_photo VARCHAR(255) NOT NULL DEFAULT '',
    cover_photo VARCHAR(255) NOT NULL DEFAULT '',
    location VARCHAR(100) NOT NULL DEFAULT '',
    bio VARCHAR(255) NOT NULL DEFAULT '',
    theme VARCHAR(10) NOT NULL DEFAULT '',
    birth_date_d INT,
    birth_date_m INT,
    birth_date_y INT,
    show_birthdate_y BOOLEAN DEFAULT TRUE,
    show_birthdate_dm BOOLEAN DEFAULT TRUE,
    status INT NOT NULL DEFAULT 0,
    verify_hash VARCHAR(16) NOT NULL DEFAULT '',
    created_at TIMESTAMP NOT NULL DEFAULT NOW()
);

CREATE TABLE follows (
    follower_id INT,
    followee_id INT,
    created_at TIMESTAMP NOT NULL DEFAULT NOW(),
    FOREIGN KEY (follower_id) REFERENCES users(id),
    FOREIGN KEY (followee_id) REFERENCES users(id),
    PRIMARY KEY (follower_id, followee_id)
);

CREATE TABLE notifications (
    id INT PRIMARY KEY AUTO_INCREMENT,
    title VARCHAR(60) NOT NULL DEFAULT '',
    content VARCHAR(80) NOT NULL DEFAULT '',
    is_read INT NOT NULL DEFAULT 0,
    created_at TIMESTAMP DEFAULT NOW(),
    user_id INT NOT NULL,
    FOREIGN KEY (user_id) REFERENCES users(id)
);

CREATE TABLE tweets (
    id INT PRIMARY KEY AUTO_INCREMENT,
    tweet_text VARCHAR(140) NOT NULL,
    tweet_type ENUM('text', 'photo', 'poll'),
    end_date TIMESTAMP NULL DEFAULT NULL,
    user_id INT NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT NOW(),
    FOREIGN KEY (user_id) REFERENCES users(id)
);

CREATE TABLE tweet_hashtags (
    tweet_id INT NOT NULL,
    hashtag_name VARCHAR(30) NOT NULL DEFAULT '',
    created_at TIMESTAMP NOT NULL DEFAULT NOW(),
    FOREIGN KEY (tweet_id) REFERENCES tweets(id),
    PRIMARY KEY (tweet_id, hashtag_name)
);

CREATE TABLE tweet_photos (
    id INT PRIMARY KEY AUTO_INCREMENT,
    image_url VARCHAR(255) NOT NULL DEFAULT '',
    tweet_id INT,
    FOREIGN KEY (tweet_id) REFERENCES tweets(id)
);

CREATE TABLE retweets (
    tweet_id INT,
    user_id INT,
    retweet_text VARCHAR(140) NOT NULL DEFAULT '',
    FOREIGN KEY (tweet_id) REFERENCES tweets(id),
    FOREIGN KEY (user_id) REFERENCES users(id),
    PRIMARY KEY (tweet_id, user_id)
);

CREATE TABLE tweet_answers (
    id INT PRIMARY KEY AUTO_INCREMENT,
    title VARCHAR(25) NOT NULL DEFAULT '',
    tweet_id INT NOT NULL,
    FOREIGN KEY (tweet_id) REFERENCES tweets(id)
);

CREATE TABLE tweet_voting_history (
    user_id INT,
    tweet_id INT,
    answer_id INT,
    created_at TIMESTAMP DEFAULT NOW(),
    FOREIGN KEY (answer_id) REFERENCES tweet_answers(id),
    FOREIGN KEY (user_id) REFERENCES users(id),
    FOREIGN KEY (tweet_id) REFERENCES tweets(id),
    PRIMARY KEY (user_id, tweet_id)
);

CREATE TABLE replies (
    id INT PRIMARY KEY AUTO_INCREMENT,
    reply_text VARCHAR(140) NOT NULL DEFAULT '',
    user_id INT NOT NULL,
    tweet_id INT NOT NULL,
    created_at TIMESTAMP DEFAULT NOW(),
    FOREIGN KEY (user_id) REFERENCES users(id),
    FOREIGN KEY (tweet_id) REFERENCES tweets(id)
);

CREATE TABLE likes (
    user_id INT,
    tweet_id INT,
    created_at TIMESTAMP DEFAULT NOW(),
    FOREIGN KEY (user_id) REFERENCES users(id),
    FOREIGN KEY (tweet_id) REFERENCES tweets(id),
    PRIMARY KEY (user_id, tweet_id)
);

CREATE TABLE direct_messages (
    id INT PRIMARY KEY AUTO_INCREMENT,
    sender_id INT NOT NULL,
    receiver_id INT NOT NULL,
    dm_text VARCHAR(255) NOT NULL DEFAULT '',
    is_read BOOLEAN NOT NULL DEFAULT FALSE,
    created_at TIMESTAMP DEFAULT NOW(),
    FOREIGN KEY (sender_id) REFERENCES users(id),
    FOREIGN KEY (receiver_id) REFERENCES users(id)
);

CREATE OR REPLACE VIEW denormalize_tweets AS
SELECT
t.id,
t.tweet_text,
t.tweet_type,
t.user_id,
t.created_at,
u.email,
u.username,
u.name,
u.profile_photo,
u.cover_photo,
u.location,
u.bio,
IF(tp.id, CONCAT('[',
		GROUP_CONCAT(
			JSON_OBJECT(
				'id',tp.id,
				'image_url',tp.image_url
			)
		), ']'
	)
, null) AS tweet_photos,
IF(ta.id,
	CONCAT('{',
	'"end": "', COALESCE(t.end_date, 0), '",',
	'"voted": ',
		CONCAT('[',
			GROUP_CONCAT(
				JSON_OBJECT(
					'id', ta.id,
					'title', ta.title,
					'voted', COALESCE((
						SELECT
							COUNT(*)
						FROM tweet_voting_history
						GROUP BY (answer_id)
						HAVING answer_id = ta.id
					), 0)
				)
			),
		']'),
	'}')
, null) AS answers,
GROUP_CONCAT(th.hashtag_name) AS hashtags,
COALESCE(rt.retweet, 0) AS "retweet", 
COALESCE(l.liked, 0) AS "liked" 
FROM tweets `t` 
LEFT JOIN (SELECT tweet_id, COUNT(*) AS "retweet" FROM retweets GROUP BY tweet_id) `rt` ON (t.id = rt.tweet_id) 
LEFT JOIN (SELECT tweet_id, COUNT(*) AS "liked" FROM likes GROUP BY tweet_id) `l` ON (t.id = l.tweet_id)
LEFT JOIN users `u` ON (t.user_id = u.id) 
LEFT JOIN tweet_photos tp ON t.id = tp.tweet_id
LEFT JOIN tweet_answers ta ON t.id = ta.tweet_id
LEFT JOIN tweet_hashtags th on t.id = th.tweet_id
GROUP BY t.id
ORDER BY t.created_at DESC

CREATE OR REPLACE VIEW denormalize_repliestwitter AS
SELECT
r.id,
r.reply_text,
r.user_id,
r.tweet_id,
r.created_at,
u.email,
u.username,
u.name,
u.profile_photo,
u.cover_photo,
u.location,
u.bio
FROM replies r 
LEFT JOIN users u ON (r.user_id = u.id) 
ORDER BY r.created_at DESC

/* INSERT DATA */

INSERT INTO `users` (`email`, `username`, `password`, `name`, `profile_photo`, `cover_photo`, `location`, `bio`, `theme`, `birth_date_d`, `birth_date_m`, `birth_date_y`, `show_birthdate_y`, `show_birthdate_dm`, `status`, `verify_hash`, `created_at`) VALUES ('panotza_ja@hotmail.com', 'panotza', '$2a$10$SPsqrcNBuWehwrIcYQFPAOPJBk2KoCDsDhc2mTekiuLsiVSe2rIya', 'Xion', '', '', '', '', '', NULL, NULL, NULL, 1, 1, 0, '', '2018-01-18 17:15:44');
INSERT INTO `users` (`email`, `username`, `password`, `name`, `profile_photo`, `cover_photo`, `location`, `bio`, `theme`, `birth_date_d`, `birth_date_m`, `birth_date_y`, `show_birthdate_y`, `show_birthdate_dm`, `status`, `verify_hash`, `created_at`) VALUES ('test@example.com', 'tester', '$2a$10$EPJBtUdJuWi8AiW5r2C1LeJNLtXJNglVAix6HWt8oSCJKA9ROrKf6', 'Tester Hajime', '', '', '', '', '', NULL, NULL, NULL, 1, 1, 0, '', '2018-01-18 17:17:04');
INSERT INTO `users` (`email`, `username`, `password`, `name`, `profile_photo`, `cover_photo`, `location`, `bio`, `theme`, `birth_date_d`, `birth_date_m`, `birth_date_y`, `show_birthdate_y`, `show_birthdate_dm`, `status`, `verify_hash`, `created_at`) VALUES ('Julien.Harber@hotmail.com', 'Graciela24', '$2a$10$E.zQcE.DqF/ZuyO3f3bUm.drvrEhCRU0GA5aL5QCC2NddLtNlG7Wi', 'Nya Mraz', '', '', '', '', '', NULL, NULL, NULL, 1, 1, 0, '', '2018-01-18 17:20:37');
INSERT INTO `users` (`email`, `username`, `password`, `name`, `profile_photo`, `cover_photo`, `location`, `bio`, `theme`, `birth_date_d`, `birth_date_m`, `birth_date_y`, `show_birthdate_y`, `show_birthdate_dm`, `status`, `verify_hash`, `created_at`) VALUES ('Destin1@hotmail.com', 'Thurman51', '$2a$10$r3fIpZy8GnqPr.Ab8rmjBO8heW8nsZxzIVy/cmpKJNhCNHZzrJQ6y', 'Joesph Brown', '', '', '', '', '', NULL, NULL, NULL, 1, 1, 0, '', '2018-01-18 17:25:28');
INSERT INTO `users` (`email`, `username`, `password`, `name`, `profile_photo`, `cover_photo`, `location`, `bio`, `theme`, `birth_date_d`, `birth_date_m`, `birth_date_y`, `show_birthdate_y`, `show_birthdate_dm`, `status`, `verify_hash`, `created_at`) VALUES ('Adriel_Dietrich@hotmail.com', 'Bart_Schiller68', '$2a$10$CP2l6O9rfNBjHH3X3xLalOMyVOrzLLmzsASnk0rZl4in6HcLbXpF6', 'Dudley Murphy', '', '', '', '', '', NULL, NULL, NULL, 1, 1, 0, '', '2018-01-18 17:25:28');
INSERT INTO `users` (`email`, `username`, `password`, `name`, `profile_photo`, `cover_photo`, `location`, `bio`, `theme`, `birth_date_d`, `birth_date_m`, `birth_date_y`, `show_birthdate_y`, `show_birthdate_dm`, `status`, `verify_hash`, `created_at`) VALUES ('Elwin.Hessel@yahoo.com', 'Brennon_Morissette', '$2a$10$0c5.NPM6m5rMVP2PV4XDa.DGlPZkBGvsj.325N0.1tYdqZKz87eE2', 'Emmet Mitchell', '', '', '', '', '', NULL, NULL, NULL, 1, 1, 0, '', '2018-01-18 17:25:29');
INSERT INTO `users` (`email`, `username`, `password`, `name`, `profile_photo`, `cover_photo`, `location`, `bio`, `theme`, `birth_date_d`, `birth_date_m`, `birth_date_y`, `show_birthdate_y`, `show_birthdate_dm`, `status`, `verify_hash`, `created_at`) VALUES ('Myrtis_Konopelski@gmail.com', 'Delia35', '$2a$10$ZIOilREKL9X7XYeAKhm6xOF6lssfjrqevY5yCJoOsxwzr2pDFhoo6', 'Katarina Heathcote', '', '', '', '', '', NULL, NULL, NULL, 1, 1, 0, '', '2018-01-18 17:25:29');
INSERT INTO `users` (`email`, `username`, `password`, `name`, `profile_photo`, `cover_photo`, `location`, `bio`, `theme`, `birth_date_d`, `birth_date_m`, `birth_date_y`, `show_birthdate_y`, `show_birthdate_dm`, `status`, `verify_hash`, `created_at`) VALUES ('Malachi23@hotmail.com', 'Marilyne_Herzog91', '$2a$10$5K2VLz/eSBUSeE3JRtKJxutYNBz8tQe0QtTik72He9SgBOlBkoeYC', 'Vernie Upton', '', '', '', '', '', NULL, NULL, NULL, 1, 1, 0, '', '2018-01-18 17:25:29');
INSERT INTO `users` (`email`, `username`, `password`, `name`, `profile_photo`, `cover_photo`, `location`, `bio`, `theme`, `birth_date_d`, `birth_date_m`, `birth_date_y`, `show_birthdate_y`, `show_birthdate_dm`, `status`, `verify_hash`, `created_at`) VALUES ('Meagan_McLaughlin46@yahoo.com', 'Demond15', '$2a$10$YFg9oPnCn42FTgDaQDJt/.NrGd9Ydq6ZLUMAPZz2D1ZOyuPjZGmVK', 'Scot Kiehn', '', '', '', '', '', NULL, NULL, NULL, 1, 1, 0, '', '2018-01-18 17:25:30');
INSERT INTO `users` (`email`, `username`, `password`, `name`, `profile_photo`, `cover_photo`, `location`, `bio`, `theme`, `birth_date_d`, `birth_date_m`, `birth_date_y`, `show_birthdate_y`, `show_birthdate_dm`, `status`, `verify_hash`, `created_at`) VALUES ('Margarita_Collier89@yahoo.com', 'Athena_Dare79', '$2a$10$7LJg3f9YN1mP6VFPjaZwWOEcJVwoID527gnvT6ygcRE19IrF8neFa', 'Kelvin Herzog DDS', '', '', '', '', '', NULL, NULL, NULL, 1, 1, 0, '', '2018-01-18 17:25:30');
INSERT INTO `users` (`email`, `username`, `password`, `name`, `profile_photo`, `cover_photo`, `location`, `bio`, `theme`, `birth_date_d`, `birth_date_m`, `birth_date_y`, `show_birthdate_y`, `show_birthdate_dm`, `status`, `verify_hash`, `created_at`) VALUES ('Carroll89@gmail.com', 'Tierra64', '$2a$10$44SE8nK.7KUo3feGQ3CK4.rbG41yCqxjBF.RU5dYdVEsLasM..8g.', 'Lorena Hansen', '', '', '', '', '', NULL, NULL, NULL, 1, 1, 0, '', '2018-01-18 17:26:21');
INSERT INTO `users` (`email`, `username`, `password`, `name`, `profile_photo`, `cover_photo`, `location`, `bio`, `theme`, `birth_date_d`, `birth_date_m`, `birth_date_y`, `show_birthdate_y`, `show_birthdate_dm`, `status`, `verify_hash`, `created_at`) VALUES ('Brandi.Dach@yahoo.com', 'Joan_McClure', '$2a$10$pgeGybNRY4yL45k.VhgV3.gR8Zo6xt1zwxu2VxjMBBsI92D5.vw0C', 'Cleveland Hand', '', '', '', '', '', NULL, NULL, NULL, 1, 1, 0, '', '2018-01-18 17:26:21');
INSERT INTO `users` (`email`, `username`, `password`, `name`, `profile_photo`, `cover_photo`, `location`, `bio`, `theme`, `birth_date_d`, `birth_date_m`, `birth_date_y`, `show_birthdate_y`, `show_birthdate_dm`, `status`, `verify_hash`, `created_at`) VALUES ('Delta3@hotmail.com', 'Jazlyn.Barton', '$2a$10$u.ujPPLl91gmqvzG9Fc5IOBz0lLmRF.YsNTsgD4gNU9giATDcePVi', 'Lisa Thiel', '', '', '', '', '', NULL, NULL, NULL, 1, 1, 0, '', '2018-01-18 17:26:21');
INSERT INTO `users` (`email`, `username`, `password`, `name`, `profile_photo`, `cover_photo`, `location`, `bio`, `theme`, `birth_date_d`, `birth_date_m`, `birth_date_y`, `show_birthdate_y`, `show_birthdate_dm`, `status`, `verify_hash`, `created_at`) VALUES ('Jonathon_Heathcote@yahoo.com', 'Elza47', '$2a$10$aOtn4efjDiEpi.l5j0Ko8u0qlTrmyjan9l/A.h00MIbMxaTTmVFiu', 'Miss Maxie Herzog', '', '', '', '', '', NULL, NULL, NULL, 1, 1, 0, '', '2018-01-18 17:26:22');
INSERT INTO `users` (`email`, `username`, `password`, `name`, `profile_photo`, `cover_photo`, `location`, `bio`, `theme`, `birth_date_d`, `birth_date_m`, `birth_date_y`, `show_birthdate_y`, `show_birthdate_dm`, `status`, `verify_hash`, `created_at`) VALUES ('Jade.Considine78@hotmail.com', 'Leilani.Zboncak72', '$2a$10$EGSWxeqlEPlMDYQ2.Od8J.NcMQ.G7FOCVKTqI34PG4NPkBZQpk8OS', 'Marisa Rolfson', '', '', '', '', '', NULL, NULL, NULL, 1, 1, 0, '', '2018-01-18 17:26:22');
INSERT INTO `users` (`email`, `username`, `password`, `name`, `profile_photo`, `cover_photo`, `location`, `bio`, `theme`, `birth_date_d`, `birth_date_m`, `birth_date_y`, `show_birthdate_y`, `show_birthdate_dm`, `status`, `verify_hash`, `created_at`) VALUES ('Esta_Bartell28@gmail.com', 'Lindsey.Predovic', '$2a$10$hhEGUsQpXwrk8mnWCxr2uOmTWD3B0Orsanz4C.9l3PXeq.902VeLK', 'Alek Turcotte III', '', '', '', '', '', NULL, NULL, NULL, 1, 1, 0, '', '2018-01-18 17:26:22');
INSERT INTO `users` (`email`, `username`, `password`, `name`, `profile_photo`, `cover_photo`, `location`, `bio`, `theme`, `birth_date_d`, `birth_date_m`, `birth_date_y`, `show_birthdate_y`, `show_birthdate_dm`, `status`, `verify_hash`, `created_at`) VALUES ('Bennie_McKenzie95@yahoo.com', 'Shana_Moore', '$2a$10$OKUTudkCgSJ6wWXhpe1pj.x8UDvhcdfnXJzc4qgA6GsWeNH56YBlm', 'Myron Strosin IV', '', '', '', '', '', NULL, NULL, NULL, 1, 1, 0, '', '2018-01-18 17:26:23');
INSERT INTO `users` (`email`, `username`, `password`, `name`, `profile_photo`, `cover_photo`, `location`, `bio`, `theme`, `birth_date_d`, `birth_date_m`, `birth_date_y`, `show_birthdate_y`, `show_birthdate_dm`, `status`, `verify_hash`, `created_at`) VALUES ('Mireille41@gmail.com', 'Enrique45', '$2a$10$5mvQWcabNoJg9sJj1DjUAuwUiM/EhEBsDO4B6CWwjF2TJOZ.mAS1a', 'Madge Jaskolski', '', '', '', '', '', NULL, NULL, NULL, 1, 1, 0, '', '2018-01-18 17:26:23');
INSERT INTO `users` (`email`, `username`, `password`, `name`, `profile_photo`, `cover_photo`, `location`, `bio`, `theme`, `birth_date_d`, `birth_date_m`, `birth_date_y`, `show_birthdate_y`, `show_birthdate_dm`, `status`, `verify_hash`, `created_at`) VALUES ('Brook_Kutch@gmail.com', 'Benny81', '$2a$10$TAtIy4Ai/ZGimEd7Bsbc5eQuqOGk3QXzzq93qSKv.fR9teVpOCR7W', 'Jamie Padberg', '', '', '', '', '', NULL, NULL, NULL, 1, 1, 0, '', '2018-01-18 17:26:23');
INSERT INTO `users` (`email`, `username`, `password`, `name`, `profile_photo`, `cover_photo`, `location`, `bio`, `theme`, `birth_date_d`, `birth_date_m`, `birth_date_y`, `show_birthdate_y`, `show_birthdate_dm`, `status`, `verify_hash`, `created_at`) VALUES ('Ellsworth_Jakubowski18@yahoo.com', 'Cloyd25', '$2a$10$b8RRCO23I16cVmnehhaVzeROaR0NmFgt23TnaoehM7JeBK5L7THPC', 'Isom White', '', '', '', '', '', NULL, NULL, NULL, 1, 1, 0, '', '2018-01-18 17:26:23');
INSERT INTO `users` (`email`, `username`, `password`, `name`, `profile_photo`, `cover_photo`, `location`, `bio`, `theme`, `birth_date_d`, `birth_date_m`, `birth_date_y`, `show_birthdate_y`, `show_birthdate_dm`, `status`, `verify_hash`, `created_at`) VALUES ('Rhea80@hotmail.com', 'Van_Nolan11', '$2a$10$dS6v4L65jUW5pZ7uZJEtEu3Pg8UvNeMp9E2ww1ABMzsNXAQnbWEiW', 'Sage Shanahan', '', '', '', '', '', NULL, NULL, NULL, 1, 1, 0, '', '2018-01-18 17:26:24');
INSERT INTO `users` (`email`, `username`, `password`, `name`, `profile_photo`, `cover_photo`, `location`, `bio`, `theme`, `birth_date_d`, `birth_date_m`, `birth_date_y`, `show_birthdate_y`, `show_birthdate_dm`, `status`, `verify_hash`, `created_at`) VALUES ('Sheldon_Ruecker8@yahoo.com', 'Annetta_Yost', '$2a$10$KmtvY4mvAfvzIBajmuuOuuUl/UkYEDGoGs6XteYiAbvA5XPnzG9G.', 'Troy Marquardt', '', '', '', '', '', NULL, NULL, NULL, 1, 1, 0, '', '2018-01-18 17:26:24');

INSERT INTO follows (follower_id, followee_id) VALUES
(1,2),
(2,1),
(3,1);

INSERT INTO direct_messages (sender_id, receiver_id, dm_text) VALUES
(1,2, 'hello1'),
(1,2, 'hello2'),
(1,2, 'hello3'),
(2,1, 'world'),
(2,1, 'world2');

INSERT INTO tweets (tweet_text, tweet_type, user_id) VALUES
('hello world1', 'text', 1),
('hello world with tags', 'text', 1),
('hello world3', 'text', 1),
('hello world with picture', 'photo', 1),
('abc', 'text', 2),
('', 'poll', 2),
('', 'poll', 3);

INSERT INTO tweet_hashtags (tweet_id, hashtag_name) VALUES
(2, '#cat'),
(2, '#dog');

INSERT INTO tweet_answers (title, tweet_id) VALUES
('Programmer', 6),
('Doctor', 6),
('Techer', 6),
('Designer', 6),
('noodle', 7),
('ramen', 7),
('sukiyaki', 7),
('rice with fish sasuce', 7);

INSERT INTO tweet_voting_history (tweet_id, user_id, answer_id) VALUES (1, 2, 1);
INSERT INTO tweet_voting_history (tweet_id, user_id, answer_id) VALUES (1, 1, 3);
INSERT INTO tweet_voting_history (tweet_id, user_id, answer_id) VALUES (1, 4, 4);
INSERT INTO tweet_voting_history (tweet_id, user_id, answer_id) VALUES (2, 3, 2);

INSERT INTO tweet_photos (image_url, tweet_id) VALUES
('/sdfsdf/sdds.jpg', 4);

INSERT INTO replies (reply_text, user_id, tweet_id) VALUES
('yo!', 2, 1),
('man!', 2, 1),
('hi!', 3, 1);

INSERT INTO retweets (retweet_text, tweet_id, user_id) VALUES
('OK!', 1, 4),
('very Good!', 1, 3);

INSERT INTO likes (user_id, tweet_id) VALUES
(1, 1),
(2, 1),
(3, 2);

INSERT INTO notifications (title, user_id) VALUES
('id 1 gave you an answer', 2),
('id 4 gave you an answer', 2),
('id 3 gave you an answer', 3);