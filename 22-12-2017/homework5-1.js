const fs = require('fs');

function saveToFile(fileName, data) {
    try {
        fs.writeFileSync(fileName, JSON.stringify(data), 'utf8');
    }
    catch (err) {
        console.error(err);
    }
}

function sumOfEyes(data) {
    let eyesObj = {"brown": 0, "green": 0, "blue": 0}
    data.forEach(person => eyesObj[person.eyeColor] += 1);
    saveToFile('homework5-1_eyes.json', eyesObj);
    return eyesObj;
}

function userFriendCount(data) {
    let people = [];
    data.forEach(person => {
        let personObj = {};
        personObj["_id"] = person._id;
        personObj["friendCount"] = person.friends.length;
        people.push(personObj);
    });
    saveToFile('homework5-1_friends.json', people);
    return people; 
}

function sumOfGender(data) {
    let genderObj = {"male": 0, "female": 0}
    data.forEach(person => genderObj[person.gender] += 1);
    saveToFile('homework5-1_gender.json', genderObj);
    return genderObj;
}

module.exports = {
    sumOfEyes,
    userFriendCount,
    sumOfGender
};
