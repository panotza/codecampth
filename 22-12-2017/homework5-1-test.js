const assert = require('assert');
const fs = require('fs');
const {sumOfEyes, userFriendCount, sumOfGender} = require('./homework5-1.js');

let peopleData = fs.readFileSync('homework1-4.json', 'utf8');
peopleData = JSON.parse(peopleData);

describe('tdd lab', function() {
  describe('#checkFileExist()', function() {
        it('should have homework5-1_eyes.json existed', function() {
            assert.deepEqual(fs.existsSync('homework5-1_eyes.json'), true);
        });
        it('should have homework5-1_gender.json existed', function() {
            assert.deepEqual(fs.existsSync('homework5-1_gender.json'), true);
        });
        it('should have homework5-1_friends.json existed', function() {
            assert.deepEqual(fs.existsSync('homework5-1_friends.json'), true);
        });
    });
    describe('#objectKey()', function() {

        it('should have same object key stucture as homework5-1_eyes.json', function() {
            let eyesColorData = fs.readFileSync('homework5-1_eyes.json', 'utf8');
                eyesColorData = JSON.parse(eyesColorData);
            let eyesColorKeys = Object.keys(eyesColorData);
            assert.deepEqual(eyesColorKeys, ["brown", "green", "blue"], 
            "Eyes object key structure is not same as homework5-1_eyes.json.");
        });
        it('should have same object key stucture as homework5-1_friends.json', function() {
            let friendsData = fs.readFileSync('homework5-1_friends.json', 'utf8');
                friendsData = JSON.parse(friendsData);
            let friendsKeys = Object.keys(friendsData[0]);
            assert.deepEqual(friendsKeys, ["_id", "friendCount"], 
            "Friends object key structure is not same as homework5-1_friends.json.");
        });
        it('should have same object key stucture as homework5-1_gender.json', function() {
            let genderData = fs.readFileSync('homework5-1_gender.json', 'utf8');
                genderData = JSON.parse(genderData);
            let genderKeys = Object.keys(genderData);
            assert.deepEqual(genderKeys, ["male", "female"],
            "Gender object key structure is not same as homework5-1_gender.json.");            
        });
    });
    describe('#userFriendCount()', function() {
        it('should have size of array input as 23', function() {
            assert.deepEqual(userFriendCount(peopleData).length, 23);
        });
    });
    describe('#sumOfEyes()', function() {
        it('should have sum of eyes as 23', function() {
            let resultEyes = sumOfEyes(peopleData);
            assert.deepEqual(resultEyes.brown + resultEyes.green + resultEyes.blue, 23);
        });
    });

    describe('#sumOfGender()', function() {
        it('should have sum of gender as 23', function() {
            let resultGender = sumOfGender(peopleData);
            assert.deepEqual(resultGender.male + resultGender.female, 23);
        });
    });
});