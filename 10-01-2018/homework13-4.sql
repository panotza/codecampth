SELECT t.title, AVG(s.salary) AS average_salary
FROM employees e
JOIN salaries s ON e.emp_no = s.emp_no
JOIN titles t ON e.emp_no = t.emp_no
GROUP BY t.title

SELECT (SELECT t.title
    FROM employees e
    JOIN salaries s ON e.emp_no = s.emp_no
    JOIN titles t ON e.emp_no = t.emp_no
    GROUP BY t.title
    ORDER BY AVG(s.salary) DESC LIMIT 1) as higest_salary_dept,
    (SELECT t.title
    FROM employees e
    JOIN salaries s ON e.emp_no = s.emp_no
    JOIN titles t ON e.emp_no = t.emp_no
    GROUP BY t.title
    ORDER BY AVG(s.salary) ASC LIMIT 1) as lowest_salary_dept

SELECT t.title, COUNT(*) as count
FROM employees e
JOIN titles t ON e.emp_no = t.emp_no
GROUP BY t.title