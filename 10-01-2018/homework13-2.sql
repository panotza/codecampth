SELECT e.student_id, s.name, SUM(c.price) AS total, COUNT(*) as amount
FROM enrolls e
JOIN students s ON e.student_id = s.id
JOIN courses c ON e.course_id = c.id
GROUP BY e.student_id

SELECT s.id, s.name, c.name AS course, max_price as higest_price_course
FROM enrolls e
JOIN courses c ON e.course_id = c.id
JOIN students s ON e.student_id = s.id
JOIN 
    (SELECT enrolls.student_id, MAX(courses.price) as max_price
    FROM enrolls
    JOIN courses ON enrolls.course_id = courses.id
    GROUP BY enrolls.student_id) t ON t.student_id = e.student_id
WHERE e.student_id = t.student_id AND c.price = max_price
GROUP BY s.id;

/*2.2 Better method มั้ง */
SELECT tmp.student_id, s.name, tmp.name, tmp.price
FROM (
    SELECT *
    FROM courses c
    JOIN enrolls e ON c.id = e.course_id
    ORDER BY e.student_id, c.price DESC
    LIMIT 18446744073709551615) tmp
JOIN students s ON tmp.student_id = s.id
GROUP BY tmp.student_id