MARIADB DEFAULT LEVEL IS REPEATABLE READ
+------------------+-------------+----------------------+----------------+
| Level            | Dirty_reads | Non_repeatable_reads | Phantoms_reads |
+------------------+-------------+----------------------+----------------+
| READ UNCOMMITTED | NO          | NO                   | NO             |
| READ COMMITTED   | OK          | NOT OK               | NOT OK         |
| REPEATABLE READ  | OK          | OK                   | OK             |
| SERIALIZABLE     | OK(Lock)    | OK(Lock)             | OK(Lock)       |
+------------------+-------------+----------------------+----------------+