SELECT e.course_id, c.name, SUM(c.price) AS total
FROM enrolls e
JOIN courses c ON e.course_id = c.id
GROUP BY e.course_id

SELECT e.student_id, s.name, SUM(c.price) AS total
FROM enrolls e
JOIN students s ON e.student_id = s.id
JOIN courses c ON e.course_id = c.id
GROUP BY e.student_id