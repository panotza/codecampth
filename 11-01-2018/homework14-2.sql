CREATE TABLE customers (
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(100) NOT NULL,
    address VARCHAR(255)
);

CREATE TABLE departments (
    dept_name VARCHAR(100) PRIMARY KEY,
    budget INT NOT NULL DEFAULT 0
);

CREATE TABLE employees (
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(100) NOT NULL,
    address VARCHAR(255),
    salary INT NOT NULL DEFAULT 0,
    dept_name VARCHAR(100) NOT NULL,
    FOREIGN KEY (dept_name) REFERENCES departments(dept_name)
);

CREATE TABLE suppliers (
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(100) NOT NULL,
    address VARCHAR(255),
    phone_number VARCHAR(10)
);

CREATE TABLE products (
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(100) NOT NULL,
    description VARCHAR(255) NOT NULL DEFAULT '',
    price INT NOT NULL DEFAULT 0,
    quantity INT NOT NULL DEFAULT 0,
    sup_id INT NOT NULL,
    FOREIGN KEY (sup_id) REFERENCES suppliers(id)
);

CREATE TABLE orders (
    id INT PRIMARY KEY AUTO_INCREMENT,
    customer_id INT NOT NULL,
    employee_id INT NOT NULL,
    created_at TIMESTAMP DEFAULT NOW(),
    FOREIGN KEY (customer_id) REFERENCES customers(id),
    FOREIGN KEY (employee_id) REFERENCES employees(id)
);

CREATE TABLE order_items (
    order_id INT NOT NULL,
    product_id INT NOT NULL,
    amount INT NOT NULL,
    discount INT NOT NULL DEFAULT 0,
    PRIMARY KEY (order_id, product_id),
    FOREIGN KEY (order_id) REFERENCES orders(id),
    FOREIGN KEY (product_id) REFERENCES products(id)
);

INSERT INTO customers (name) VALUES ('cus a'), ('cus b');

INSERT INTO departments (dept_name, budget) VALUES ('dept a', 1000), ('dept b', 2000);

INSERT INTO employees (name, dept_name) VALUES ('a','dept a'), ('b','dept b');

INSERT INTO suppliers (name) VALUES ('sup a'), ('sup b');

INSERT INTO products (name, price, quantity, sup_id) VALUES ('product a', 122, 23, 1), ('product b', 122, 23, 2);

INSERT INTO orders (customer_id, employee_id) VALUES (1,1);