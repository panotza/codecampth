DROP DATABASE IF EXISTS twitter;
CREATE DATABASE IF NOT EXISTS twitter;
USE twitter;

CREATE TABLE users (
    id INT PRIMARY KEY AUTO_INCREMENT,
    email VARCHAR(255) NOT NULL UNIQUE,
    username VARCHAR(255) NOT NULL UNIQUE,
    password VARCHAR(255) NOT NULL,
    name VARCHAR(100) NOT NULL DEFAULT '',
    avatar_url VARCHAR(255) NOT NULL DEFAULT '',
    cover_url VARCHAR(255) NOT NULL DEFAULT '',
    location VARCHAR(255) NOT NULL DEFAULT '',
    bio VARCHAR(255) NOT NULL DEFAULT '',
    birthday DATETIME,
    status INT NOT NULL DEFAULT 0,
    verify_hash VARCHAR(16) NOT NULL DEFAULT '',
    created_at TIMESTAMP DEFAULT NOW()
);

CREATE TABLE privacy_rules (
    user_id INT PRIMARY KEY,
    show_birth_day BOOLEAN DEFAULT TRUE,
    show_birth_year BOOLEAN DEFAULT TRUE,
    FOREIGN KEY (user_id) REFERENCES users(id)
);

CREATE TABLE followers (
    follower_id INT,
    followee_id INT,
    FOREIGN KEY (follower_id) REFERENCES users(id),
    FOREIGN KEY (followee_id) REFERENCES users(id),
    PRIMARY KEY (follower_id, followee_id)
);

CREATE TABLE notifications (
    id INT PRIMARY KEY AUTO_INCREMENT,
    title VARCHAR(255) NOT NULL DEFAULT '',
    status INT NOT NULL DEFAULT 0,
    created_at TIMESTAMP DEFAULT NOW(),
    user_id INT,
    FOREIGN KEY (user_id) REFERENCES users(id)
);

CREATE TABLE tweets (
    id INT PRIMARY KEY AUTO_INCREMENT,
    tweet_text VARCHAR(140) NOT NULL,
    tweet_type ENUM('text', 'photo', 'poll'),
    to_date TIMESTAMP NULL DEFAULT NULL,
    user_id INT,
    created_at TIMESTAMP DEFAULT NOW(),
    FOREIGN KEY (user_id) REFERENCES users(id)
);

CREATE TABLE tweet_tags (
    tweet_id INT NOT NULL,
    tag_name VARCHAR(100) NOT NULL DEFAULT '',
    FOREIGN KEY (tweet_id) REFERENCES tweets(id)
);

CREATE TABLE tweet_photos (
    id INT PRIMARY KEY AUTO_INCREMENT,
    image_url VARCHAR(255) NOT NULL DEFAULT '',
    tweet_id INT,
    FOREIGN KEY (tweet_id) REFERENCES tweets(id)
);

CREATE TABLE retweets (
    tweet_id INT,
    user_id INT,
    retweet_text VARCHAR(140) NOT NULL DEFAULT '',
    FOREIGN KEY (tweet_id) REFERENCES tweets(id),
    FOREIGN KEY (user_id) REFERENCES users(id),
    PRIMARY KEY (tweet_id, user_id)
);

CREATE TABLE tweet_answers (
    id INT PRIMARY KEY AUTO_INCREMENT,
    title VARCHAR(100) NOT NULL DEFAULT '',
    tweet_id INT NOT NULL,
    FOREIGN KEY (tweet_id) REFERENCES tweets(id)
);

CREATE TABLE tweet_voting_history (
    user_id INT,
    tweet_id INT,
    answer_id INT NOT NULL,
    created_at TIMESTAMP DEFAULT NOW(),
    FOREIGN KEY (answer_id) REFERENCES tweet_answers(id),
    FOREIGN KEY (user_id) REFERENCES users(id),
    FOREIGN KEY (tweet_id) REFERENCES tweets(id),
    PRIMARY KEY (user_id, tweet_id)
);

CREATE TABLE replies (
    id INT PRIMARY KEY AUTO_INCREMENT,
    reply_text VARCHAR(140) NOT NULL DEFAULT '',
    user_id INT NOT NULL,
    tweet_id INT NOT NULL,
    created_at TIMESTAMP DEFAULT NOW(),
    FOREIGN KEY (user_id) REFERENCES users(id),
    FOREIGN KEY (tweet_id) REFERENCES tweets(id)
);

CREATE TABLE likes (
    user_id INT,
    tweet_id INT,
    created_at TIMESTAMP DEFAULT NOW(),
    FOREIGN KEY (user_id) REFERENCES users(id),
    FOREIGN KEY (tweet_id) REFERENCES tweets(id),
    PRIMARY KEY (user_id, tweet_id)
);

CREATE TABLE direct_messages (
    id INT PRIMARY KEY AUTO_INCREMENT,
    sender_id INT NOT NULL,
    receiver_id INT NOT NULL,
    dm_text VARCHAR(255) NOT NULL DEFAULT '',
    created_at TIMESTAMP DEFAULT NOW(),
    FOREIGN KEY (sender_id) REFERENCES users(id),
    FOREIGN KEY (receiver_id) REFERENCES users(id)
);

/* INSERT DATA */

INSERT INTO users (email, username, password, avatar_url) VALUES
('example@gmail.com', 'example', 'sf3sdfsf4323', 'http://sdfdsf.com/image.jpg'),
('asd@gmail.com', 'vsas', 'sf3sdfs4323', 'http://sdfdsf.com/image.jpg'),
('asdh@gmail.com', 'sssdmple', 'asd', 'http://sdfdsf.com/sd.jpg'),
('hfd@gmail.com', 'zxdf', 'asd', 'http://sdfdsf.com/s.jpg');

INSERT INTO privacy_rules (user_id, show_birth_day, show_birth_year) VALUES
(1, FALSE, FALSE),
(2, TRUE, FALSE),
(3, FALSE, TRUE),
(4, TRUE, TRUE);

INSERT INTO followers (follower_id, followee_id) VALUES
(1,2),
(2,1),
(3,1);

INSERT INTO direct_messages (sender_id, receiver_id, dm_text) VALUES
(1,2, 'hello1'),
(1,2, 'hello2'),
(1,2, 'hello3'),
(2,1, 'world'),
(2,1, 'world2');

INSERT INTO tweets (tweet_text, tweet_type, user_id) VALUES
('hello world1', 'text', 1),
('hello world with tags', 'text', 1),
('hello world3', 'text', 1),
('hello world with picture', 'photo', 1),
('abc', 'text', 2),
('', 'poll', 2),
('', 'poll', 3);

INSERT INTO tweet_tags (tweet_id, tag_name) VALUES
(2, '#cat'),
(2, '#dog');

INSERT INTO tweet_answers (title, tweet_id) VALUES
('Programmer', 6),
('Doctor', 6),
('Techer', 6),
('Designer', 6),
('noodle', 7),
('ramen', 7),
('sukiyaki', 7),
('rice with fish sasuce', 7);

INSERT INTO tweet_voting_history (tweet_id, user_id, answer_id) VALUES (1, 2, 1);
INSERT INTO tweet_voting_history (tweet_id, user_id, answer_id) VALUES (1, 1, 3);
INSERT INTO tweet_voting_history (tweet_id, user_id, answer_id) VALUES (1, 4, 4);
INSERT INTO tweet_voting_history (tweet_id, user_id, answer_id) VALUES (2, 3, 2);

INSERT INTO tweet_photos (image_url, tweet_id) VALUES
('/sdfsdf/sdds.jpg', 4);

INSERT INTO replies (reply_text, user_id, tweet_id) VALUES
('yo!', 2, 1),
('man!', 2, 1),
('hi!', 3, 1);

INSERT INTO retweets (retweet_text, tweet_id, user_id) VALUES
('OK!', 1, 4),
('very Good!', 1, 3);

INSERT INTO likes (user_id, tweet_id) VALUES
(1, 1),
(2, 1),
(3, 2);

INSERT INTO notifications (title, user_id) VALUES
('id 1 gave you an answer', 2),
('id 4 gave you an answer', 2),
('id 3 gave you an answer', 3);