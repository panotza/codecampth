class User {
    constructor (db, row) {
        row = row || {};
        this._db = db;
        this.id = row.id || undefined;
        this.firstName = row.first_name || '';
        this.lastName = row.first_name || '';
        this.username = row.username || '';
    }
    async save () {
        if (!this._id) {
            const result = this._db.execute(`
                INSERT INTO users (
                    first_name, last_name, username
                ) VALUES (
                    ?, ?, ?
                )
            `, [this.firstName, this.lastName, this.username]);
            this.id = result.insertId;
            return;
        }
        return this._db.execute(`
            UPDATE users
            SET
            first_name = ?,
            last_name = ?,
            username = ?,
            WHERE id = ?
        `, [this.firstName, this.lastName, this.username, this.id]);
    }
    remove () {
        return this._db.execute(`
            DELETE FROM users WHERE id = ?
        `, [this.id]);
    }
    async find (id) {
        try {
            const [rows] = await this._db.execute(`
                SELECT
                    id, first_name, last_name, username
                FROM users
                WHERE id = ?
            `, [id]);
            return new User(this._db, rows[0]);
        } catch (err) {
            console.log(err);
        }
    }
    async findAll () {
        const [rows] = await this._db.execute(`
            SELECT
                id, first_name, last_name, username
            FROM users
        `);
        return rows.map((row) => new User(this._db, row));
    }
    async findByUsername (username) {
        const [rows] = await this._db.execute(`
            SELECT
                id, first_name, last_name, username
            FROM users
            WHERE username = ?
        `, [username]);
        return new User(this._db, rows[0]);
    }
}

module.exports = User;