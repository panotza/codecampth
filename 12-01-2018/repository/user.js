function createEntity (row) {
    return {
        id: row.id,
        firstName: row.first_name,
        lastName: row.last_name,
        username: row.username
    }
}
async function find (db, id) {
    const [rows] = await db.execute(`
        SELECT
            id, first_name, last_name, username
        FROM users
        WHERE id = ?
        `, [id]);
        return createEntity(rows[0]);
}
async function findAll (db) {
    const [rows] = await db.execute(`
        SELECT
            id, first_name, last_name, username
        FROM users
    `);
    return rows.map(createEntity);
}
async function findByUsername (db, username) {
    const [rows] = await db.execute(`
        SELECT
            id, first_name, last_name, username
        FROM users
        WHERE username = ?
        `, [username]);
    return createEntity(rows[0]);
}
async function store (db, user) {
    if (!user.id) {
        const result = await db.execute(`
            INSERT INTO users (
                first_name, last_name, username
            ) VALUES (
            ?, ?, ?
            )
        `, [user.firstName, user.lastName, user.username]);
        user.id = result.insertId;
    return;
    }
    return db.execute(`
        UPDATE users
        SET
            first_name = ?,
            last_name = ?,
            username = ?
        WHERE id = ?
        `, [user.firstName, user.lastName, user.username, user.id]);
}
function remove (db, id) {
    return db.execute(`
        DELETE FROM users WHERE id = ?
    `, [id]);
}

module.exports = {
    createEntity,
    find,
    findAll,
    findByUsername,
    store,
    remove
}