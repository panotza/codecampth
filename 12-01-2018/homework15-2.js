const DB = require('./lib/db');
const User = require('./repository/user');
const config = require('./config/db1');

(async () => {
    const db = await DB(config);
    const user1 = await User.find(db, 4);
    
    const xxxUsername = await User.findByUsername(db, 'xxx');
    const newUser = {
        firstName: 'New',
        lastName: 'New Last',
        username: 'new_new'
    }
    await User.store(db, newUser);
    await User.remove(db, 11);
    user1.firstName = 'tester';
    const users = await User.findAll(db);
    console.log('user1:', user1);
    console.log('all user:', users);
    console.log('xxx:', xxxUsername);
})();