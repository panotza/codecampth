function createUser (user) {
    return {
        id: user.id,
        email: user.email,
        username: user.username,
        password: user.password,
        name: user.name,
        avatarUrl: user.avatar_url,
        coverUrl: user.cover_url,
        location: user.location,
        bio: user.bio,
        birthday: user.birthday,
        status: user.status,
        verifyHash: user.verify_hash,
        createdAt: user.created_at
    };
}

module.exports = {
    async find (db, id) {
        const [rows] = await db.execute(`
            SELECT
                id,
                email,
                username,
                password,
                name,
                avatar_url,
                cover_url,
                location,
                bio,
                birthday,
                status,
                verify_hash,
                created_at
            FROM users
            WHERE id = ?
            `, [id]);
            return createUser(rows[0]);
    },
    async findAll (db) {
        const [rows] = await db.execute(`
            SELECT
                id,
                email,
                username,
                password,
                name,
                avatar_url,
                cover_url,
                location,
                bio,
                birthday,
                status,
                verify_hash,
                created_at
            FROM users
            `);
        return rows.map(createUser);
    },
    async findByUsername (db, username) {
        const [rows] = await db.execute(`
            SELECT
                id,
                email,
                username,
                password,
                name,
                avatar_url,
                cover_url,
                location,
                bio,
                birthday,
                status,
                verify_hash,
                created_at
            FROM users
            WHERE username = ?
            `, [username]);
        return createUser(rows[0]);
    },
    async store (db, user) {
        try {
            const result = await db.execute(`
                INSERT INTO users (
                    email,
                    username,
                    password,
                    name,
                    avatar_url,
                    cover_url,
                    location,
                    bio,
                    birthday,
                    verify_hash
                ) VALUES (
                ?, ?, ?, ?, ?, ?, ?, ?, ?, ?
                )
            `, [user.email, 
                user.username,
                user.password,
                user.name,
                user.avatarUrl,
                user.coverUrl,
                user.location,
                user.bio,
                user.birthday,
                user.verifyHash
            ]);
            return result[0].insertId;
        } catch (err) {
            return err;
        }
    },
    update (db, user) {
        return db.execute(`
            UPDATE users
            SET
                email = ?,
                username = ?,
                password = ?,
                name = ?,
                avatar_url = ?,
                cover_url = ?,
                location = ?,
                bio = ?,
                birthday = ?,
                status = ?,
                verify_hash = ?
            WHERE id = ?
            `, [user.email, 
                user.username,
                user.password,
                user.name,
                user.avatarUrl,
                user.coverUrl,
                user.location,
                user.bio,
                user.birthday,
                user.status,
                user.verifyHash,
                user.id
        ]);
    },
    remove (db, id) {
        return db.execute(`
            DELETE FROM users WHERE id = ?
        `, [id]);
    }
}