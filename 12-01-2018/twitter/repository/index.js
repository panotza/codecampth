module.exports = {
    user: require('./user'),
    tweet: require('./tweet'),
    like: require('./like'),
    follower: require('./follower')
}