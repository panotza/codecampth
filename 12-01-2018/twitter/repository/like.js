module.exports = {
    like (db, user_id, tweet_id) {
        return db.execute(`
            INSERT INTO likes (user_id, tweet_id) VALUES
            (?, ?)
        `, [user_id, tweet_id]);
    },
    unlike (db, user_id, tweet_id) {
        return db.execute(`
            DELETE FROM likes
            WHERE user_id = ? AND tweet_id = ?
        `, [user_id, tweet_id]);
    },
    async likeCount (db, user_id) {
        const [rows] = await db.execute(`
            SELECT COUNT(*) as likeCount
            FROM likes
            WHERE user_id = ?
            `, [user_id]);
        return rows[0].likeCount;
    }
}