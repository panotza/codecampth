const photo = require('./tweet_photo');

function createTweet (tweet) {
    return {
        id: tweet.id,
        text: tweet.tweet_text,
        type: tweet.tweet_type,
        toDate: tweet.to_date,
        userId: tweet.user_id,
        createdAt: tweet.created_at,
        replyCount: tweet.reply_count,
        retweetCount: tweet.retweet_count,
        likeCount: tweet.like_count
    };
}

module.exports = {
    async find (db, id) {
        const [rows] = await db.execute(`
            SELECT
                id,
                tweet_text,
                tweet_type,
                to_date,
                user_id,
                created_at
            FROM tweets
            WHERE id = ?
            `, [id]);
        return createTweet(rows[0]);
    },
    async findByUserId (db, user_id) {
        const [rows] = await db.execute(`
            SELECT
                id,
                tweet_text,
                tweet_type,
                to_date,
                user_id,
                created_at
            FROM tweets
            WHERE user_id = ?
            `, [user_id]);
        return rows.map(createTweet);
    },
    async findAll (db) {
        const [rows] = await db.execute(`
            SELECT 
                t.id,
                t.tweet_text,
                t.tweet_type,
                t.to_date,
                t.user_id,
                t.created_at,
                r.reply_count,
                rt.retweet_count,
                l.like_count
            FROM tweets t
            LEFT JOIN (
					SELECT tweet_id, COUNT(*) reply_count
					FROM replies
					GROUP BY tweet_id
					) as r ON t.id = r.tweet_id
            LEFT JOIN (
					SELECT tweet_id, COUNT(*) as retweet_count
					FROM retweets
					GROUP BY tweet_id
					) as rt ON t.id = rt.tweet_id
			LEFT JOIN (
					SELECT tweet_id, COUNT(*) as like_count
					FROM likes
					GROUP BY tweet_id
					) as l ON t.id = l.tweet_id
            GROUP BY t.id
            `);
        return Promise.all(
            rows
            .map(createTweet)
            .map(async tweet => {
                return {
                    ...tweet, 
                    photos: await photo.findByTweetId(db, tweet.id)
                }
            })
        );
    },
    async store (db, tweet) {
        try {
            const result = await db.execute(`
                INSERT INTO tweets (
                    tweet_text,
                    tweet_type,
                    to_date,
                    user_id
                ) VALUES (
                    ?, ?, ?, ?
                )
            `, [tweet.text, 
                tweet.type,
                tweet.toDate,
                tweet.userId
            ]);
        return result[0].insertId;
        } catch (err) {
            return err;
        }
    },
    remove (db, id) {
        return db.execute(`
            DELETE FROM users WHERE id = ?
        `, [id]);
    }
}