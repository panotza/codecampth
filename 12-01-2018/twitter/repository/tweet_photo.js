function createTweetPhoto (photo) {
    return {
        id: photo.id,
        imageUrl: photo.image_url
    };
}

module.exports = {
    async findByTweetId (db, tweet_id) {
        const [rows] = await db.execute(`
            SELECT
                id,
                image_url
            FROM tweet_photos
            WHERE tweet_id = ?
            `, [tweet_id]);
        return rows.map(createTweetPhoto);
    }
}