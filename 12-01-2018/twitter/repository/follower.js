module.exports = {
    async showfollowing (db, user_id) {
        const [rows] = await db.execute(`
            SELECT followee_id as followeeId
            FROM followers
            WHERE follower_id = ?
            `, [user_id]);
        return rows;
    },
    follow (db, follower_id, followee_id) {
        return db.execute(`
            INSERT INTO followers (follower_id, followee_id) VALUES
            (?, ?)
        `, [follower_id, followee_id]);
    },
    unfollow (db, follower_id, followee_id) {
        return db.execute(`
            DELETE FROM followers
            WHERE follower_id = ? AND followee_id = ?
        `, [follower_id, followee_id]);
    },
    async isFollowing (db, follower_id, followee_id) {
        const [rows] = await db.execute(`
            SELECT COUNT(*) as isFollowing
            FROM followers
            WHERE follower_id = ? AND followee_id = ?
            `, [follower_id, followee_id]);
        return rows[0].isFollowing;
    },
    async followingCount (db, user_id) {
        const [rows] = await db.execute(`
            SELECT COUNT(*) as followCount
            FROM followers
            WHERE follower_id = ?
            `, [user_id]);
        return rows[0].followCount;
    },
    async followeeCount (db, user_id) {
        const [rows] = await db.execute(`
            SELECT COUNT(*) as followeeCount
            FROM followers
            WHERE followee_id = ?
            `, [user_id]);
        return rows[0].followeeCount;
    }
}