const state = {
    products: [
        {
            id: 1,
            title: 'CHERPRANG',
            image: 'https://bnk48-www-html.s3-ap-southeast-1.amazonaws.com/uploads/members/file-0341204af64d796fb3c715fd86ba4262.png',
            salePrice: 100,
            promotionDate: 0,
        },
        {
            id: 2,
            title: 'JAN',
            image: 'https://bnk48-www-html.s3-ap-southeast-1.amazonaws.com/uploads/members/file-d4e0083b4123be76c45fa5665b49a1b8.png',
            salePrice: 100,
            promotionDate: 0,
        },
        {
            id: 3,
            title: 'JENNIS',
            image: 'https://bnk48-www-html.s3-ap-southeast-1.amazonaws.com/uploads/members/file-9c4230866e3f5ccaa911f728e36091cd.png',
            salePrice: 100,
            promotionDate: 0,
        },
        {
            id: 4,
            title: 'KAEW',
            image: 'https://bnk48-www-html.s3-ap-southeast-1.amazonaws.com/uploads/members/file-c2564d5b05b43648969b2d88b395883b.png',
            salePrice: 100,
            promotionDate: 0,
        }
    ],
    nextId: 5
}

window.onload = function() {
    initEvent();
    initProduct();
};

function initProduct() {
    window.productList = document.getElementById("productList");
    state.products.forEach(p => {
        window.productList.innerHTML += addProductWrapper(p.id, p.title, p.image, p.salePrice);
    });
}

function initEvent() {
    const saveProductBtn = document.getElementById('saveProductBtn');
    const addProductBtn = document.getElementById('addProductBtn');
    const bookId = document.getElementById('bookId');
    const bookTitle = document.getElementById('bookTitle');
    const bookSalePrice = document.getElementById('bookSalePrice');
    const bookPromotionDate = document.getElementById('bookPromotionDate');
    const bookImage = document.getElementById('bookImage');
    const productMethod = document.getElementById('productMethod');

    addProductBtn.addEventListener('click', () => {
        bookId.value = state.nextId;
        bookTitle.value = '';
        bookSalePrice.value = 0;
        bookPromotionDate.value = '';
        bookImage.value = '';
        productMethod.value = 'add';

        if (bookId.hasAttribute('disabled')) {
            bookId.removeAttribute('disabled');
        }
    });

    saveProductBtn.addEventListener('click', () => {
        const product = {
            id: +bookId.value,
            title: bookTitle.value,
            image: bookImage.value,
            salePrice: +bookSalePrice.value,
            promotionDate: bookPromotionDate.value
        }

        if (productMethod.value === 'add') {
            saveProduct(product);
        } else if (productMethod.value === 'edit') {
            editProduct(product);
        }

        bookId.value = '';
        bookTitle.value = '';
        bookSalePrice.value = 0;
        bookPromotionDate.value = '';
        bookImage.value = '';
        $('#productModal').modal('hide');
    });
}

function saveProduct(product) {
    state.products.push(product);
    state.nextId++;
    window.productList.innerHTML += addProductWrapper(product.id, product.title, product.image, product.salePrice);
}

function editProduct(product) {
    const divProduct = document.getElementById('product-' + product.id);
    const i = findProductIndexbyId(product.id, state.products);
    if (i === undefined) return;
    state.products[i] = product;
    divProduct.innerHTML = addProduct(product.id, product.title, product.image, product.salePrice);
}

function addProductWrapper(id, title, image, price) {
    return `
    <div id="product-${id}" class="col-xs-6 col-md-3 nospace">` +
    addProduct(id, title, image, price) +
    `</div>`;
}

function addProduct(id, title, image, price) {
    return `
    <div class="card" style="overflow: hidden;">
        <h2>${title}</h2>
        <a href="#">
            <img class="card-img" src="${image}"
                alt="${title}">
        </a>
        <p>price: ${price}THB
            <a href="#" class="btn btn-success" onClick="handleEditProduct(${id})">
                <i class="fa fa-pencil" aria-hidden="true"></i>
            </a>
            <a href="#" class="btn btn-danger" onClick="handleDeleteProduct(${id})">
                <i class="fa fa-eraser" aria-hidden="true"></i>
            </a>
        </p>
    </div>`;
}

function handleEditProduct(id) {
    const i = findProductIndexbyId(id, state.products);
    if (i === undefined) return;
    const product = state.products[i];
    const disabledAtt = document.createAttribute("disabled");
    const bookId = document.getElementById('bookId');
    bookId.value = product.id;
    bookId.setAttributeNode(disabledAtt);
    document.getElementById('bookTitle').value = product.title;
    document.getElementById('bookSalePrice').value = product.salePrice;
    document.getElementById('bookPromotionDate').value = product.promotionDate;
    document.getElementById('bookImage').value = product.image;
    document.getElementById('productMethod').value = 'edit';
    $('#productModal').modal('show');
}

function handleDeleteProduct(id) {
    const product = document.getElementById('product-' + id);
    const result = confirm('ArproductMethode you sure to delete item : ' + id);
    if (result) {
        window.productList.removeChild(product);
        state.products = state.products.filter(p =>
            p.id !== id
            ? true
            : false
        );
    }
}

function findProductIndexbyId(id, products) {
    let index;
    for(i = 0; i < products.length; i++) {
        if (products[i].id === id) {
            return i;
        }
    }
    return;
}

const addCart = new addCartFactory();
const timeId = setInterval(addCart, 5000);
const cartAmount = document.getElementById('cartAmount');

function addCartFactory() {
    let count = 0;
    return function() {
        cartAmount.innerText = ++count;
    }
}