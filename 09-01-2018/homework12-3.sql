SELECT DISTINCT c.name, i.name AS teach_by, c.price
FROM enrolls e
JOIN courses c ON e.course_id = c.id
JOIN instructors i ON c.teach_by = i.id
ORDER BY c.id;

SELECT c.name, i.name AS teach_by, c.price
FROM courses c
LEFT JOIN enrolls e ON c.id = e.course_id
LEFT JOIN instructors i ON c.teach_by = i.id
WHERE e.student_id IS NULL
ORDER BY c.id;

SELECT c.name, i.name AS teach_by, c.price
FROM courses c
LEFT JOIN enrolls e ON c.id = e.course_id
JOIN instructors i ON c.teach_by = i.id
WHERE e.student_id IS NULL
ORDER BY c.id;