INSERT INTO students (name) VALUES 
('Ronald'),
('Thomas'),
('Katrina'),
('Gracie'),
('Cathy'),
('Sanford'),
('Sabrina'),
('Aaron'),
('Tyler'),
('Kenneth');

INSERT INTO enrolls (student_id, course_id) VALUES 
(1, 1),
(1, 2),
(2, 3),
(2, 4),
(5, 1),
(6, 1),
(1, 3),
(6, 2),
(9, 2),
(5, 3);

SELECT DISTINCT c.name
FROM enrolls e
JOIN courses c ON e.course_id = c.id
ORDER BY c.id;

SELECT c.name
FROM courses c
LEFT JOIN enrolls e ON c.id = e.course_id
WHERE e.student_id IS NULL
ORDER BY c.id;