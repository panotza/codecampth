SELECT i.name
FROM instructors i
LEFT JOIN courses c ON i.id = c.teach_by
WHERE c.name IS NULL;

SELECT c.name
FROM instructors i
RIGHT JOIN courses c ON i.id = c.teach_by
WHERE c.teach_by IS NULL;