SELECT e.first_name,
       dept_name,
       e2.first_name as manager
FROM employees e
LEFT JOIN dept_emp de ON e.emp_no = de.emp_no
LEFT JOIN departments d ON de.dept_no = d.dept_no
LEFT JOIN dept_manager dm ON d.dept_no = dm.dept_no
LEFT JOIN employees e2 ON dm.emp_no = e2.emp_no
ORDER BY e.emp_no limit 100;

SELECT CONCAT(e.first_name, ' ', e.last_name) as full_name,
       dept_name,
       s.salary,
       CONCAT(e2.first_name, ' ', e2.last_name) as manager,
       s2.salary
FROM employees e
LEFT JOIN dept_emp de ON e.emp_no = de.emp_no
LEFT JOIN departments d ON de.dept_no = d.dept_no
LEFT JOIN dept_manager dm ON d.dept_no = dm.dept_no
LEFT JOIN employees e2 ON dm.emp_no = e2.emp_no
LEFT JOIN salaries s ON e.emp_no = s.emp_no
LEFT JOIN salaries s2 ON e2.emp_no = s2.emp_no
WHERE s.salary > s2.salary
GROUP BY e.emp_no
limit 100;