const DB = require('./lib/db');

const mySQLconfig = {
    "host": "localhost",
    "port": 3306,
    "user": "root",
    "password": "",
    "database": "test"
}
class session {
    constructor(){
        this.connectDB();
    }

    async connectDB(ctx, next) {
        this.connection = await DB(mySQLconfig);
    }
    /**
     * get session object by key
     */
    async get (key, maxAge, { rolling }) {
        const [rows] = await this.connection.execute(`SELECT * FROM _mysql_session_store WHERE id = ?`, [key]);
        return JSON.parse(rows[0].data);
    }

    /**
     * set session object for key, with a maxAge (in ms)
     */
    async set (key, sess, maxAge, { rolling }) {
        const row = await this.connection.execute(`
            INSERT INTO _mysql_session_store
                (id, expires, data)
            VALUES
                (?, ?, ?)
            ON DUPLICATE KEY UPDATE
                expires = ?,
                data = ?
        `, [key, maxAge, JSON.stringify(sess),
            maxAge, JSON.stringify(sess)]);
    }

    /**
     * destroy session for key
     */
    async destroy (key) {
        await this.connection.execute(`
            DELETE FROM _mysql_session_store WHERE id = ?
        `, [key]);
    }
}

module.exports = session;