class Todo {
    constructor(db, title = '', completed = false, id) {
        this._db = db || undefined;
        this._id = id || undefined;
        this._title = title;
        this._completed = completed;
    }
    get () {
        return {
            id: this._id, 
            title: this._title, 
            completed: this._completed
        };
    }
    async findById (id) {
        const [rows] = await this._db.execute(`
            SELECT
                id, title, completed
            FROM todos
            WHERE id = ?
        `, [id]);
        if (!rows[0]) {
            return false;
        }
        this._id = rows[0].id;
        this._title = rows[0].title;
        this._completed = rows[0].completed;
        return this.get();
    }
    async getTodos () {
        const [rows] = await this._db.execute(`
            SELECT
                id, title, completed
            FROM todos
            ORDER BY id DESC
        `);
        return rows.map(t => {
            const todo = new Todo(this._db, t.title, t.completed, t.id);
            return todo.get();
        });
    }
    setComplete (completed) {
        this._completed = completed;
    }
    async save () {
        if (!this._id) {
            const [rows] = await this._db.execute(`
                INSERT INTO todos (title, completed) VALUES
                (?, ?)
            `, [this._title, this._completed ? 1 : 0]);
            if (!rows.affectedRows) {
                return false;
            }
            this._id = rows.insertId;
            return this.get();
        } else {
            const [rows] = await this._db.execute(`
                UPDATE todos SET 
                title = ?,
                completed = ?
                WHERE id = ? 
            `, [this._title, this._completed ? 1 : 0, this._id]);
            if (!rows.affectedRows) {
                return false;
            } else {
                return this.findById(this._id);
            }
        }
    }
    async remove () {
        if (!this._id) {
            return false;
        } else {
            const [rows] = await this._db.execute(`
                DELETE FROM todos
                WHERE id = ? 
            `, [this._id]);
            return rows.affectedRows;
        }
    }
}

module.exports = Todo;