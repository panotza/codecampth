const Koa = require('koa');
const bodyParser = require('koa-bodyparser');
const json = require('koa-json');
const config = require('./config/db');
const DB = require('./lib/db');

new Koa()
    .use(connectDatabase)
    .use(bodyParser())
    .use(json())
    .use(validate)
    .use(require('./routes/todos').routes())
    .listen(3000);

async function connectDatabase (ctx, next) {
    ctx.state.db = await DB(config);
    await next();
}
 async function validate (ctx, next) {
    if (Object.keys(ctx.request.body).length > 0) {
        const {title, completed} = ctx.request.body;
        if (typeof title !== 'string' || title.length === 0 || title.length > 200) {
            ctx.throw(400, 'title must be string and not empty and not more than 200 charactors.');
        }
        if (completed && completed !== 'true' && completed !== 'false') {
            ctx.throw(400, 'completed must be true or false');
        }
    }
    await next();
}