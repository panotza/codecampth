const Todo = require('../models/todo');

module.exports = {
    async listTodos (ctx) {
        const db = ctx.state.db;
        const todo = new Todo(db);
        const todos = await todo.getTodos();
        ctx.body = todos;
    },
    async createTodo (ctx) { 
        const db = ctx.state.db;
        const {title, completed} = ctx.request.body;
        const todo = new Todo(db, title, completed);
        let result = await todo.save();
        if (!result) {
            ctx.throw(400, 'cannot create new todo.');
        }
        ctx.body = todo.get();
    },
    async getTodo (ctx) {
        const db = ctx.state.db;
        const todo = new Todo(db);
        let result = await todo.findById(ctx.params.id);
        if (!result) {
            ctx.throw(404, 'todo not found');
        }
        ctx.body = result;
    },
    async updateTodo (ctx) { 
        const db = ctx.state.db;
        const {title, completed} = ctx.request.body;
        const todo = new Todo(db, title, completed, ctx.params.id);
        let result = await todo.save();
        if (!result) {
            ctx.throw(404, 'todo not found');
        }
        ctx.body = result;
    },
    async deleteTodo (ctx) { 
        const db = ctx.state.db;
        const todo = new Todo(db, undefined, undefined, ctx.params.id);
        let result = await todo.remove();
        if (!result) {
            ctx.throw(404, 'todo not found');
        }
        ctx.body = 'successfully deleted.';
    },
    async completeTodo (ctx) { 
        const db = ctx.state.db;
        const todo = new Todo(db);
        let result = await todo.findById(ctx.params.id);
        if (!result) {
            ctx.throw(404, 'todo not found');
        }
        todo.setComplete(true);
        ctx.body = await todo.save();
    },
    async incompleteTodo (ctx) { 
        const db = ctx.state.db;
        const todo = new Todo(db);
        let result = await todo.findById(ctx.params.id);
        if (!result) {
            ctx.throw(404, 'todo not found');
        }
        todo.setComplete(false);
        ctx.body = await todo.save();
    }
}