const router = require('koa-router')();
const helpers = require("../helpers/todos");

router.param('id', (id, ctx, next) => {
    if (id && isNaN(id)) {
        ctx.throw(404, 'id must be number.');
    }
    return next();
});
router
    .prefix('/api')
    .get('/todo', helpers.listTodos)
    .post('/todo', helpers.createTodo)
    .get('/todo/:id', helpers.getTodo)
    .patch('/todo/:id', helpers.updateTodo)
    .delete('/todo/:id', helpers.deleteTodo)
    .put('/todo/:id/complete', helpers.completeTodo)
    .delete('/todo/:id/complete', helpers.incompleteTodo);

module.exports = router;