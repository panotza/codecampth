import React from 'react';

const Welcome = () => (
  <div className="welcome-form">
    <h1>Welcome to Twitter.</h1>
    <h3>Connect with your friends — and other fascinating people. <br /> Get in-the-moment updates on the things that interest you. <br /> And watch events unfold, in real time, from every angle.
    </h3>
  </div>
);

export default Welcome;