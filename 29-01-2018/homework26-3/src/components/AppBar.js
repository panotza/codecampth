import React from 'react';

const AppBar = () => (
  <div>
    <nav className="navbar is-fixed-top">
      <div className="navbar-brand">
        <a className="navbar-item logo" href="https://bulma.io">
          <i className="fa fa-twitter" aria-hidden="true"></i>
        </a>
        <div className="navbar-burger burger" data-target="navbarExampleTransparentExample">
          <span></span>
          <span></span>
          <span></span>
        </div>
      </div>

      <div id="navbarExampleTransparentExample" className="navbar-menu">
        <div className="navbar-start">
          <a className="navbar-item" href="https://bulma.io/">
            Home
        </a>
          <a className="navbar-item" href="https://bulma.io/">
            About
        </a>
        </div>

        <div className="navbar-end">
          <div className="navbar-item">
            <div className="field is-grouped">
              <div className="navbar-item has-dropdown is-hoverable">
                <a className="navbar-link" href="/documentation/overview/start/">
                  Language: English
                </a>
                <div className="navbar-dropdown is-boxed">
                  <a className="navbar-item" href="/documentation/overview/start/">
                    Overview
                </a>
                  <a className="navbar-item" href="https://bulma.io/documentation/modifiers/syntax/">
                    Modifiers
                </a>
                  <a className="navbar-item" href="https://bulma.io/documentation/columns/basics/">
                    Columns
                </a>
                  <a className="navbar-item" href="https://bulma.io/documentation/layout/container/">
                    Layout
                </a>
                  <a className="navbar-item" href="https://bulma.io/documentation/form/general/">
                    Form
                </a>
                  <hr className="navbar-divider" />
                  <a className="navbar-item" href="https://bulma.io/documentation/elements/box/">
                    Elements
                </a>
                  <a className="navbar-item is-active" href="https://bulma.io/documentation/components/breadcrumb/">
                    Components
                </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </nav>
  </div>
);

export default AppBar;