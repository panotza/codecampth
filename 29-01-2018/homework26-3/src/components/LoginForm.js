import React from 'react';

const LoginForm = () => (
  <div className="login-form">
    <div className="card">
      <div className="card-content">
        <div className="field">
          <div className="control">
            <input className="input is-small" type="text" placeholder="Phone, email or username" />
          </div>
        </div>
        <div class="field is-grouped">
          <p class="control is-expanded">
            <input class="input is-small" type="text" placeholder="Find a repository" />
          </p>
          <p class="control">
            <a class="button is-rounded is-small">
              Log in
            </a>
          </p>
        </div>
        <label class="checkbox">
          <input type="checkbox" />
          Remember me · <a href="#">Forgot password?</a>
        </label>
      </div>
    </div>
    <br />
    <div className="card">
      <header class="card-header">
        <p class="card-header-title">
          New to Twitter? Sign up
        </p>
      </header>
      <div className="card-content">
        <div className="field">
          <div className="control">
            <input className="input is-small" type="text" placeholder="Full name" />
          </div>
        </div>
        <div className="field">
          <div className="control">
            <input className="input is-small" type="text" placeholder="Email" />
          </div>
        </div>
        <div className="field">
          <div className="control">
            <input className="input is-small" type="text" placeholder="Password" />
          </div>
        </div>
        <div class="field is-grouped is-grouped-right">
          <p class="control">
            <a class="button is-info is-rounded">
              Sign up for Twitter
            </a>
          </p>
        </div>
      </div>
    </div>
  </div>

);

export default LoginForm;