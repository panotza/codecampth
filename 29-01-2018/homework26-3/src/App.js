import React, { Component } from 'react';
import AppBar from './components/AppBar';
import LoginForm from './components/LoginForm';
import Welcome from './components/Welcome';
import 'bulma/css/bulma.css';
import 'font-awesome/css/font-awesome.min.css';
import './App.css';

class App extends Component {
  render() {
    return (
      <div>
        <AppBar />        
        <div className="bg-img">
          <Welcome />
          <LoginForm />
        </div>
      </div>
    );
  }
}

export default App;
