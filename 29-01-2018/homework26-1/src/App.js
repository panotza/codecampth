import React, { Component } from 'react';
import { Card, Button, Input, Form, Icon } from 'antd';
import './App.css';

const FormItem = Form.Item;

class App extends Component {
  render() {
    return (
      <div>
        <h1 style={{ textAlign: 'center', marginTop: 70}}>Welecome to my application</h1>
        <Card style={{ width: 300, margin: '0 auto'}}>
          <h3>Sign In</h3>
          <hr />
          <FormItem>
            <h4>Username:</h4>
            <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Username" />
            <h4>Password:</h4>
            <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Password" />          
          </FormItem>
          <Button
            type="primary"
            htmlType="submit"
          >
            Sign In
          </Button>
          <p>
            <a className="login-form-forgot" href="">Forgot password</a>
          </p>
          <hr />
          <h3>New User</h3>
          <Button
            type="primary"
            htmlType="submit"
          >
            Sign Up
          </Button>
        </Card>
      </div>
    );
  }
}

export default App;
