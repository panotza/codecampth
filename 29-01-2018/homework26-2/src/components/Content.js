import React from 'react';
import { Card, Row, Col, Rate } from 'antd';

const Content = () => (
  <Row gutter={16}>
    <Col span={6}>
      <Card>
        <div className="custom-image">
          <img alt="example" width="100%" src="https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png" />
        </div>
        <div className="custom-card">
          <h3>Europe Street beat</h3>
          <p>www.instagram.com</p>
          <p><Rate /></p>
        </div>
      </Card>
    </Col>
    <Col span={6}>
      <Card>
        <div className="custom-image">
          <img alt="example" width="100%" src="https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png" />
        </div>
        <div className="custom-card">
          <h3>Europe Street beat</h3>
          <p>www.instagram.com</p>
          <p><Rate /></p>
        </div>
      </Card>
    </Col>
    <Col span={6}>
      <Card>
        <div className="custom-image">
          <img alt="example" width="100%" src="https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png" />
        </div>
        <div className="custom-card">
          <h3>Europe Street beat</h3>
          <p>www.instagram.com</p>
          <p><Rate /></p>
        </div>
      </Card>
    </Col>
    <Col span={6}>
      <Card>
        <div className="custom-image">
          <img alt="example" width="100%" src="https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png" />
        </div>
        <div className="custom-card">
          <h3>Europe Street beat</h3>
          <p>www.instagram.com</p>
          <p><Rate /></p>
        </div>
      </Card>
    </Col>
    <style jsx global>{`
      .custom-image img {
        display: block;
      }
      .custom-card {
        padding: 10px 0px;
      }
      .custom-card p {
        color: #999;
      }
    `}</style>
  </Row>
);

export default Content;