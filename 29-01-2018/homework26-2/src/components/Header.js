import React from 'react';
import { Menu, Carousel } from 'antd'

const Header = () => (
  <div className="header">
    <Menu
      mode="horizontal"
      theme="dark"
    >
      <Menu.Item key="product">
        <h3>Product</h3>
      </Menu.Item>
      <Menu.Item key="About">
        <h3>About</h3>
      </Menu.Item>
    </Menu>
    <Carousel autoplay>
      <div><h3>1</h3></div>
      <div><h3>2</h3></div>
      <div><h3>3</h3></div>
      <div><h3>4</h3></div>
    </Carousel>
    <style jsx global>{`
      .ant-carousel .slick-slide {
        text-align: center;
        height: 160px;
        line-height: 160px;
        background: #367279;
        overflow: hidden;
      }
          
      .ant-carousel .slick-slide h3 {
        color: #fff;
      }
    `}</style>
  </div>
);

export default Header;