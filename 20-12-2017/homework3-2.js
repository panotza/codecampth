const fs = require('fs');

function readFileByFileName(fileName) {
    return new Promise((resolve, reject) => {
        fs.readFile('file/' + fileName, 'utf8', function(err, data) {
            if (err)
                reject(err);
            else
                resolve(data);
        });
    });
}

async function readAndCreateNewFile() {
    try {
        let dataHead = await readFileByFileName('head.txt');
        let dataBody = await readFileByFileName('body.txt');
        let dataLeg = await readFileByFileName('leg.txt');
        let dataFeet = await readFileByFileName('feet.txt');
        let fullData = dataHead + '\n' + dataBody + '\n' + dataLeg + '\n' + dataFeet;
        
        fs.writeFile('robot.txt', fullData, 'utf8', err => {
            if (err)
                console.error(err);
            else
                console.log("writing file completed.");
        });
    } catch (err) {
        console.error(err);
    }
}

readAndCreateNewFile();