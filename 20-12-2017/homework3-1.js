const fs = require('fs');

function readFileByFileName(fileName) {
    return new Promise((resolve, reject) => {
        fs.readFile('file/' + fileName, 'utf8', function(err, data) {
            if (err)
                reject(err);
            else
                resolve(data);
        });
    });
}

let readFilePromises = [
    readFileByFileName('head.txt'),
    readFileByFileName('body.txt'),
    readFileByFileName('leg.txt'),
    readFileByFileName('feet.txt')
];

Promise.all(readFilePromises)
.then((data) => {
    let fullData = data.join('\n');
    fs.writeFile('robot.txt', fullData, 'utf8', err => {
        if (err)
            console.error(err);
        else
            console.log("writing file completed.");
    });
})
.catch(err => {
    console.error(err);
});