const fs = require('fs');
const name = 8;
const gender = 9;
const company = 10;
const email = 11;
const friends = 19;

let data = fs.readFileSync('homework1-4.json', 'utf8');
let people = JSON.parse(data);

let newPeople = [];
people.forEach(person => {
    let keys = Object.keys(person);
    let objRow = {};
    objRow[keys[name]] = person[keys[name]];
    objRow[keys[gender]] = person[keys[gender]];
    objRow[keys[company]] = person[keys[company]];
    objRow[keys[email]] = person[keys[email]];
    objRow[keys[friends]] = person[keys[friends]];
    newPeople.push(objRow);
});
console.log(newPeople);